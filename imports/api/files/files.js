/* eslint-disable consistent-return */

const createThumb = function (fileObj, readStream, writeStream) {
  gm(readStream, fileObj.name()).resize('256', '256').stream().pipe(writeStream);
};

const createMedium = function (fileObj, readStream, writeStream) {
  gm(readStream, fileObj.name()).resize('800', '800').stream().pipe(writeStream);
};

Files = new FS.Collection('files', {
  stores: [
      // new FS.Store.GridFS("thumbs", { transformWrite: createThumb }),
      // new FS.Store.GridFS("medium", { transformWrite: createMedium }),
      new FS.Store.GridFS('uploads')
  ]
});
export default Files;
