import Files from '../files';

Files.allow({
    insert() {
        // add custom authentication code here
        return true;
    },
    update() {
        // add custom authentication code here
        return true;
    },
    remove() {
        // add custom authentication code here
        return true;
    },
    download(userId, fileObj) {
        return true;
    }
});
Meteor.publish('files.list', () => Files.find());

Meteor.publish('files.view', (_id) => {
    check(_id, String);
    return Files.find(_id);
});
