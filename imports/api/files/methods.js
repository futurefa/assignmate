import { Meteor } from 'meteor/meteor';
import Files from './files';

Meteor.methods({
    saveFile(id, file) {
        // check(id, Match.Optional(String));
        check(file, Object);
        Files.insert(file, (err, fileObj) => {
            if (err) {
                console.log(err); //in case there is an error, log it to the console
            } else {
                console.log('saved ', fileObj);
                return fileObj;
                //the image upload is done successfully.
                //you can use this callback to add the id of your file into another collection
                //for this you can use fileObj._id to get the id of the file
            }
        });
    },
    removeFile(id) {
        check(id, String);
        return Files.remove(id);
    }
});
