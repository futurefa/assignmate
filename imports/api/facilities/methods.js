import { Meteor } from 'meteor/meteor';
import Facilities from './facilities';
import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';

Meteor.methods({
    saveFacility(id, facility) {
        check(id, Match.Optional(String));
        check(facility, Object);
        const facilityWithoutContacts = _.omit(facility, ['contacts']);
        let contacts = facility.contacts || false;
        const isValid = Facilities.simpleSchema().namedContext().validate(facilityWithoutContacts, { modifier: false });
        if (isValid) {
            const resp = Facilities.upsert({ _id: id }, { $set: facilityWithoutContacts }, { validate: false });
            if (resp && contacts) {
                const resId = id || (resp ? resp.insertedId : undefined);
                contacts = contactsWithId(contacts);
                Facilities.update(
                    resId,
                    { $set: { contacts } },
                    { validate: false }
                );
            }
            return resp;
        } 
            throw new Meteor.Error('Facility data is not valid');
    },
    removeFacility(id) {
        check(id, String);
        return Facilities.remove(id);
    }
});

function contactsWithId(contacts) {
    return _.compact(_.map(contacts, (contact) => {
        if (contact.contactId) {
            return contact;
        } 
            contact.contactId = Meteor.uuid();
            return contact;
    }));
}
