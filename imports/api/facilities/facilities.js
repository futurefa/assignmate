import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

const Facilities = new Mongo.Collection('Facilities');
export default Facilities;

Facilities.allow({
    insert: () => false,
    update: () => false,
    remove: () => false,
});

Facilities.deny({
    insert: () => true,
    update: () => true,
    remove: () => true,
});

Facilities.allow({
    insert: () => false,
    update: () => false,
    remove: () => false,
});

Facilities.deny({
    insert: () => true,
    update: () => true,
    remove: () => true,
});

Facilities.schema = new SimpleSchema({
    facilityName: {
        type: String,
        label: 'Name of the facility.',
        optional: true
    },
    address1: {
        type: String,
        label: 'Mailing Address 1',
        optional: true
    },
    address2: {
        type: String,
        label: 'Mailing Address 2',
        optional: true
    },
    streetName: {
        type: String,
        label: 'Street Name',
        optional: true
    },
    customerID: {
        type: String,
        optional: true
    },
    city: {
        type: String,
        max: 5,
        optional: true
    },
    state: {
        type: String,
        max: 2,
        optional: true
    },
    zipCode: {
        type: String,
        optional: true
    },
    active: {
        type: Boolean,
        optional: true
    },
    accountNumber: {
        type: String,
        optional: true
    },
    storeNumber: {
        type: String,
        optional: true
    },
    waterAgency: {
        type: String,
        optional: true
    },
    facilityType: {
        type: String,
        optional: true,
        allowedValues: ['Residential', 'Commercial', 'Hospital', 'School']
    },
    hazardLevel: {
        type: String,
        optional: true
    },
    testMonth: {
        type: Date,
        optional: true
    },
    surveyCycle: {
        type: String,
        optional: true
    },
    nextSurvey: {
        type: Date,
        optional: true
    },
    propertyIDNumber: {
        type: String,
        optional: true
    },
    comment: {
        type: String,
        optional: true,
        max: 256
    },
    miscellaneous: {
        type: new SimpleSchema({
            contactName: {
                type: String,
                optional: true
            },
            phoneNumber: {
                type: String,
                optional: true
            },
        }),
        optional: true,
    },
    streetNumber: {
        type: String,
        optional: true
    },
    contactName: {
        type: String,
        optional: true
    },
    phone: {
        type: String,
        optional: true
    },
    fax: {
        type: String,
        optional: true
    },
    otherPhone: {
        type: String,
        optional: true
    },
    email: {
        type: String,
        optional: true
    },
    noOfUnits: {
        type: String,
        optional: true
    },
    mapCode: {
        type: String,
        optional: true
    },
    salutation: {
        type: String,
        optional: true,
        allowedValues: ['Mr', 'Mrs', 'Ms']
    },
    additionalInformation: {
        type: Object,
        optional: true
    },
    'additionalInformation.contactName': {
        type: String,
        optional: true
    },
    'additionalInformation.phone': {
        type: String,
        optional: true
    },
    'additionalInformation.fax': {
        type: String,
        optional: true
    },
    'additionalInformation.otherPhone': {
        type: String,
        optional: true
    },
    'additionalInformation.email': {
        type: String,
        optional: true
    },
    'additionalInformation.addressLastUpdated': {
        type: String,
        optional: true
    },
    'additionalInformation.noOfUnits': {
        type: String,
        optional: true
    },
    'additionalInformation.mapCode': {
        type: String,
        optional: true
    },
    'additionalInformation.customerSalutation': {
        type: String,
        optional: true,
        allowedValues: ['Mr', 'Mrs', 'Ms']
    },
    contacts: {
        type: Array,
        optional: true
    },
    'contacts.$': {
        type: Object,
        optional: true,
        blackbox: true
    },
    'contacts.$.contactId': {
        type: String,
        optional: true
    },
    'contacts.$.type': {
        type: String,
        optional: true
    },
    'contacts.$.mailingName': {
        type: String,
        optional: true
    },
    'contacts.$.address1': {
        type: String,
        optional: true
    },
    'contacts.$.address2': {
        type: String,
        optional: true
    },
    'contacts.$.city': {
        type: String,
        optional: true
    },
    'contacts.$.state': {
        type: String,
        optional: true
    },
    'contacts.$.zipCode': {
        type: String,
        optional: true
    },
    'contacts.$.salutation': {
        type: String,
        optional: true,
        allowedValues: ['Mr', 'Mrs', 'Ms']
    },
    'contacts.$.contactName': {
        type: String,
        optional: true
    },
    'contacts.$.email': {
        type: String,
        optional: true
    },
    'contacts.$.comments': {
        type: String,
        optional: true
    },
    'contacts.$.active': {
        type: Boolean,
        optional: true
    },
    'contacts.$.phone': {
        type: String,
        optional: true
    }
});

Facilities.attachSchema(Facilities.schema);
