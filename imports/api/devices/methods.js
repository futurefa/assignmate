import { Meteor } from 'meteor/meteor';
import Devices from './devices';
import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';

Meteor.methods({
    saveDevice(id, device) {
        check(id, Match.Optional(String));
        check(device, Object);
        const isValid = Devices.simpleSchema().namedContext().validate(device, { modifier: false });
        if (isValid) {
            return Devices.upsert({ _id: id }, { $set: device }, { validate: false });
        } 
            throw new Meteor.Error('Device data is not valid');
    },
    removeDevice(id) {
        check(id, String);
        return Devices.remove(id);
    }
});
