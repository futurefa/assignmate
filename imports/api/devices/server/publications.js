import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Devices from '../devices';
import Facilities from '../../facilities/facilities';
import Customers from '../../customers/customers';

const moment = require('moment');

Meteor.publish('devices.list', () => Devices.find());

Meteor.publish('devices.view', (_id) => {
    check(_id, String);
    return Devices.find(_id);
});

Meteor.publish('devicesduefortests.list', () => {
    const startDate = moment().startOf('day').toDate();
    const endDate = moment().add(50, 'days').toDate();
    const devices = Devices.find({ nextTestDue: { $gte: startDate, $lt: endDate } });
    const facilityIds = devices.map((device) => {
        if (device.facilityId) { return device.facilityId; }
    });
    const facilities = Facilities.find({ _id: { $in: facilityIds } });
    const customerIds = facilities.map((facility) => {
        if (facility.customerID) { return facility.customerID; }
    });
    return [
        devices,
        facilities,
        Customers.find({ _id: { $in: customerIds } }),
    ];
});
