import { Meteor } from 'meteor/meteor';
import Teams from './teams';
import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';

Meteor.methods({
  saveTeam(id, team) {
    check(id, Match.Optional(String));
    check(team, Object);
    const teamWithoutMembers = _.omit(team, ['members']);
    let members = team.members || false;
    const isValid = Teams.simpleSchema().namedContext().validate(teamWithoutMembers, { modifier: false });
    if (isValid) {
      const resp = Teams.upsert({ _id: id }, { $set: teamWithoutMembers }, { validate: false });
      if (resp && members) {
        const resId = id || (resp ? resp.insertedId : undefined);
        members = membersWithId(members);
        Teams.update(
                    resId,
                    { $set: { members } },
                    { validate: false }
                );
      }
      return resp;
    } 
      throw new Meteor.Error('Team data is not valid');
  },
  removeTeam(id) {
    check(id, String);
    return Teams.remove(id);
  }
});

function membersWithId(members) {
  return _.compact(_.map(members, (member) => {
    if (member._id) {
      return member;
    } 
      member._id = Meteor.uuid();
      return member;
  }));
}
