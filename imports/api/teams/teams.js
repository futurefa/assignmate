import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

const Teams = new Mongo.Collection('Teams');
export default Teams;

Teams.allow({
  insert: () => false,
  update: () => false,
  remove: () => false
});

Teams.deny({
  insert: () => true,
  update: () => true,
  remove: () => true
});

Teams.allow({
  insert: () => false,
  update: () => false,
  remove: () => false
});

Teams.deny({
  insert: () => true,
  update: () => true,
  remove: () => true
});

Teams.schema = new SimpleSchema({
  name: {
    type: String,
    label: 'Name of the team.',
    optional: true
  },
  ownerId: {
    type: String,
    optional: true
  },
  active: {
    type: Boolean,
    optional: true
  },
  description: {
    type: String,
    label: 'What is the team about.',
    optional: true,
    max: 256
  },
  miscellaneous: {
    type: new SimpleSchema({
      contactName: {
        type: String,
        optional: true
      },
      phoneNumber: {
        type: String,
        optional: true
      }
    }),
    optional: true
  },
  phone: {
    type: String,
    optional: true
  },
  email: {
    type: String,
    optional: true
  },
  // members belonging to this team
  members: {
    type: Array,
    optional: true
  },
  'members.$': {
    type: Object,
    optional: true,
    blackbox: true
  },
  'members.$._id': {
    type: String,
    optional: true
  },
  'members.$.name': {
    type: String,
    optional: true
  },
  'members.$.active': {
    type: Boolean,
    optional: true
  },
  //people to contact about this team
  contacts: {
        type: Array,
        optional: true
    },
    'contacts.$': {
        type: Object,
        optional: true,
        blackbox: true
    },
    'contacts.$._id': {
        type: String,
        optional: true
    },
    'contacts.$.contactName': {
        type: String,
        optional: true
    },
    'contacts.$.salutation': {
        type: String,
        optional: true,
        allowedValues: ['Mr', 'Mrs', 'Ms']
    },
    'contacts.$.email': {
        type: String,
        optional: true
    },
    'contacts.$.phone': {
        type: String,
        optional: true
    },
    'contacts.$.comments': {
        type: String,
        optional: true
    },
    'contacts.$.active': {
        type: Boolean,
        optional: true
    },
});

Teams.attachSchema(Teams.schema);
