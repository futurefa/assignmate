import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Teams from '../teams';

Meteor.publish('teams.list', () => Teams.find());

Meteor.publish('teams.view', (_id) => {
  check(_id, String);
  return Teams.find(_id);
});

Meteor.publish('teams.view.members', (_id) => {
  check(_id, String);
  const team = Teams.findOne(_id);
  const ids = team && team.contacts && _.pluck(team.contacts, '_id');
  const selector = { _id: { $in: ids } };
  return Meteor.users.find(selector);
});
