import { Meteor } from 'meteor/meteor';
import Company from './company';
import SimpleSchema from 'simpl-schema';
import { ValidatedMethod, ValidationError } from 'meteor/mdg:validated-method';

Meteor.methods({
  saveCompany(id, company) {
    check(id, Match.Optional(String));
    check(company, Object);

    const isValid = Company.simpleSchema().namedContext().validate(company, { modifier: false });
    if (isValid) {
      return Company.upsert({ _id: id }, { $set: company }, { validate: false });
    } 
      throw new Meteor.Error('Company data is not valid');
  },
  removeCompany(id) {
    check(id, String);
    return Company.remove(id);
  }
});

export const removeCompany = new ValidatedMethod({
  name: 'companies.remove',
  validate: new SimpleSchema({
    _id: { type: String }
  }).validator(),
  run({ _id }) {
    Company.remove(_id);
  }
});
