import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

const Company = new Mongo.Collection('Company');
export default Company;

Company.allow({
    insert: () => false,
    update: () => false,
    remove: () => false,
});

Company.deny({
    insert: () => true,
    update: () => true,
    remove: () => true,
});

Company.schema = new SimpleSchema({
    ///////////////////////
    //General Company Info
    //////////////////////
    companyName: {
        type: String,
        label: 'Name of the company.',
        optional: true
    },
    address1: {
        type: String,
        label: 'Mailing Address 1',
        optional: true
    },
    address2: {
        type: String,
        label: 'Mailing Address 2',
        optional: true
    },
    city: {
        type: String,
        max: 5,
        optional: true
    },
    state: {
        type: String,
        max: 2,
        optional: true
    },
    zipCode: {
        type: String,
        optional: true
    },
    active: {
        type: Boolean,
        optional: true
    },
    contactPerson: {
        type: String,
        optional: true
    },
    phone: {
        type: String,
        optional: true
    },
    otherPhone: {
        type: String,
        optional: true
    },
    fax: {
        type: String,
        optional: true
    },
    email: {
        type: String,
        optional: true
    },
    businessLicenceNo: {
        type: String,
        optional: true
    },
    licenceExpiry: {
        type: Date,
        optional: true
    },
    comments: {
        type: String,
        optional: true
    },
    ////////////////////////////////
    //Company Insurance Information
    ///////////////////////////////
    insuranceInformation: {
        type: Object,
        optional: true
    },
    'insuranceInformation.insuranceCompany': {
        type: String,
        optional: true
    },
    'insuranceInformation.contactName': {
        type: String,
        optional: true
    },
    'insuranceInformation.phone': {
        type: String,
        optional: true
    },
    'insuranceInformation.fax': {
        type: String,
        optional: true
    },
    'insuranceInformation.policyNo': {
        type: String,
        optional: true
    },
    'insuranceInformation.liabilityAmount': {
        type: String,
        optional: true
    },
    'insuranceInformation.expiryDate': {
        type: Date,
        optional: true
    },
    'insuranceInformation.comments': {
        type: String,
        optional: true
    },
});

Company.attachSchema(Company.schema);
