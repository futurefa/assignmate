import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Company from '../company';

Meteor.publish('company.viewAll', () => Company.find());

Meteor.publish('company.view', (_id) => {
    check(_id, String);
    return Company.find(_id);
});
