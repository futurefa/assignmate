import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Testings from '../testings';

Meteor.publish('testings.list', () => Testings.find());

Meteor.publish('testings.view', (_id) => {
    check(_id, String);
    return Testings.find(_id);
});
