import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

const Testings = new Mongo.Collection('Testings');
export default Testings;

Testings.allow({
    insert: () => false,
    update: () => false,
    remove: () => false,
});

Testings.deny({
    insert: () => true,
    update: () => true,
    remove: () => true,
});

Testings.schema = new SimpleSchema({
    installId: {
        type: String,
        optional: true
    },
    month: {
        type: Date,
        label: 'Test Month/Cycle',
        optional: true
    },
    type: {
        type: String,
        label: 'Test Type',
        optional: true
    },
    pressureUnit: {
        type: String,
        label: 'Pressure Unit',
        allowedValues: ['Psi', 'kPa'],
        optional: true
    },
    linePressure: {
        type: String,
        optional: true
    },
    meterReading: {
        type: String,
        optional: true
    },
    repairSummary: {
        type: String,
        optional: true
    },
    serviceRestored: {
        type: Boolean,
        optional: true
    },
    airGapOk: {
        type: Boolean,
        label: 'Air Gap Ok',
        optional: true
    },
    testDate: {
        type: Date,
        optional: true
    },
    status: {
        type: String,
        allowedValues: ['PASS', 'Fail', 'INCOMPLETE'],
        label: 'Pass',
        optional: true
    },
    nextDate: {
        type: Date,
        label: 'Next test date',
        optional: true
    },
    testerName: {
        type: String,
        label: 'Name of Tester',
        optional: true
    },
    certNo: {
        type: String,
        label: 'Certificate Number',
        optional: true
    },
    gaugeNo: {
        type: String,
        label: 'Gauge number of test kit',
        optional: true
    },
    calibrationDate: {
        type: Date,
        label: 'Calibration Date',
        optional: true
    },
    timeIn: {
        type: Date,
        optional: true,
    },
    timeOut: {
        type: Date,
        optional: true
    },
    sent: {
        type: Date,
        label: 'Date test sent',
        optional: true
    },
    printed: {
        type: Boolean,
        label: 'Test printed',
        optional: true
    },
    remarks: {
        type: String,
        label: 'Tester remarks',
        optional: true
    },
    comments: {
        type: String,
        label: 'Comments',
        optional: true
    },
});

Testings.attachSchema(Testings.schema);
