import { Meteor } from 'meteor/meteor';
import Testings from './testings';
import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';

Meteor.methods({
    saveTest(id, test) {
        check(id, Match.Optional(String));
        check(test, Object);
        const isValid = Testings.simpleSchema().namedContext().validate(test, { modifier: false });
        if (isValid) {
            return Testings.upsert({ _id: id }, { $set: test }, { validate: false });
        } 
            throw new Meteor.Error('Test data is not valid');
    },
    removeTest(id) {
        check(id, String);
        return Testings.remove(id);
    }
});

export const removeTest = new ValidatedMethod({
  name: 'testing.remove',
  validate: new SimpleSchema({
    _id: { type: String },
  }).validator(),
  run({ _id }) {
    Testings.remove(_id);
  },
});
