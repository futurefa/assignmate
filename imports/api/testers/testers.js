import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

const Testers = new Mongo.Collection('Testers');
export default Testers;

Testers.allow({
    insert: () => false,
    update: () => false,
    remove: () => false,
});

Testers.deny({
    insert: () => true,
    update: () => true,
    remove: () => true,
});

Testers.schema = new SimpleSchema({
    certNumber: {
        type: String,
        label: 'Certificate number.',
        optional: true
    },
    name: {
        type: String,
        label: 'Tester name',
        optional: true
    },
    companyName: {
        type: String,
        label: 'Tester company name',
        optional: true
    },
    mainPhone: {
        type: String,
        label: 'Main phone number',
        optional: true
    },
    otherPhone: {
        type: String,
        label: 'Other phone number',
        optional: true
    },
    fax: {
        type: String,
        label: 'Testers fax number',
        optional: true
    },
    email: {
        type: String,
        label: 'Testers email',
        optional: true
    },
    address1: {
        type: String,
        label: 'Testers address one',
        optional: true
    },
    address2: {
        type: String,
        label: 'Address two',
        optional: true
    },
    city: {
        type: String,
        label: 'City',
        optional: true
    },
    state: {
        type: String,
        max: 2,
        label: 'State of residence',
        optional: true
    },
    zipCode: {
        type: String,
        optional: true,
        label: 'Zip code of residence',
        max: 5,
    },
    certExp: {
        type: Date,
        label: 'Certificate expiration.',
        optional: true
    },
    tester: {
        type: Boolean,
        label: 'If certified as a tester',
        optional: true
    },
    inspector: {
        type: Boolean,
        label: 'If certified as a inspector',
        optional: true
    },
    installer: {
        type: Boolean,
        label: 'If certified as a installer',
        optional: true
    },
    active: {
        type: Boolean,
        optional: true
    },
    comment: {
        type: String,
        optional: true
    },
    calibratedBy: {
        type: String,
        label: 'Company that provided calibration service',
        optional: true
    },
    calibrationExp: {
        type: Date,
        label: 'Expiry date test kit due for new calibration',
        optional: true
    },
    make: {
        type: String,
        label: 'Type of test kit',
        optional: true
    },
    model: {
        type: String,
        label: 'Model of test kit',
        optional: true
    },
    serialNo: {
        type: String,
        label: 'Serial number of test kit',
        optional: true
    }
});

Testers.attachSchema(Testers.schema);
