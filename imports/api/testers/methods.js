import { Meteor } from 'meteor/meteor';
import Testers from './testers';
import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';

Meteor.methods({
    saveTester(id, tester) {
        check(id, Match.Optional(String));
        check(tester, Object);
        const isValid = Testers.simpleSchema().namedContext().validate(tester, { modifier: false });
        if (isValid) {
            return Testers.upsert({ _id: id }, { $set: tester }, { validate: false });
        } 
            throw new Meteor.Error('Tester data is not valid');
    },
    removeTester(id) {
        check(id, String);
        return Testers.remove(id);
    }
});
