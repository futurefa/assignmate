import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Tasks from '../tasks';
import Teams from '../../teams/teams';

Meteor.publish('tasks.list', () => Tasks.find());

Meteor.publish('tasks.view', (_id) => {
  check(_id, String);
  return Tasks.find(_id);
});

Meteor.publish('tasks.view.team', (_id) => {
  check(_id, String);
  const task = Tasks.findOne(_id);
  return Teams.find(task.team && task.team._id);
});
