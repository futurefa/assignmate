import { Meteor } from 'meteor/meteor';
import Tasks from './tasks';
import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';

Meteor.methods({
  saveTask(id, task) {
    check(id, Match.Optional(String));
    check(task, Object);
    const taskWithoutAttachments = _.omit(task, ['attachments']);
    let attachments = task.attachments || false;
    const isValid = Tasks.simpleSchema().namedContext().validate(taskWithoutAttachments, { modifier: false });
    // if (isValid) {
      const resp = Tasks.upsert({ _id: id }, { $set: taskWithoutAttachments }, { validate: false });
      if (resp && attachments) {
        const resId = id || (resp ? resp.insertedId : undefined);
        attachments = attachmentsWithId(attachments);
        Tasks.update(
                    resId,
                    { $set: { attachments } },
                    { validate: false }
                );
      }
      return resp;
    // } else {
    //   throw new Meteor.Error('Task data is not valid')
    // }
  },
  removeTask(id) {
    check(id, String);
    return Tasks.remove(id);
  }
});

function attachmentsWithId(attachments) {
  return _.compact(_.map(attachments, (attachment) => {
    if (attachment._id) {
      return attachment;
    } 
      attachment._id = Random.id();
      return attachment;
  }));
}
