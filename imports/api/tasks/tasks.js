/* eslint-disable consistent-return */

import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

const Tasks = new Mongo.Collection('Tasks');
export default Tasks;
Tasks.allow({
  insert: () => false,
  update: () => false,
  remove: () => false
});

Tasks.deny({
  insert: () => true,
  update: () => true,
  remove: () => true
});

Tasks.allow({
  insert: () => false,
  update: () => false,
  remove: () => false
});

Tasks.deny({
  insert: () => true,
  update: () => true,
  remove: () => true
});

Tasks.schema = new SimpleSchema({
  owner: {
    type: Object,
    label: 'The ID of the user this task belongs to.',
    optional: true,
    blackbox: true
  },
  createdAt: {
    type: String,
    label: 'The date this task was created.',
    autoValue() {
      if (this.isInsert) return (new Date()).toISOString();
    }
  },
  updatedAt: {
    type: String,
    label: 'The date this task was last updated.',
    autoValue() {
      if (this.isInsert || this.isUpdate) return (new Date()).toISOString();
    }
  },
  title: {
    type: String,
    label: 'The title of the task.',
    optional: true
  },
  body: {
    type: String,
    label: 'The body of the task.',
    optional: true
  },
  dueDate: {
    type: String,
    label: 'The due date of the task.',
    optional: true
  },
  team: {
    type: Object,
    label: 'The team where the task belongs.',
    optional: true,
    blackbox: true
  },
  attachments: {
    type: Array,
    label: 'The attachments to this task.',
    optional: true
  },
  active: {
        type: Boolean,
        optional: true
  },
});

Tasks.attachSchema(Tasks.schema);
