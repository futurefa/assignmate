import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';

Meteor.methods({
    saveUser: (id, user) => {
        check(id, Match.Optional(String));
        check(user, Object);
        const isValid = Meteor.users.simpleSchema().namedContext().validate(user, { modifier: false });
        if (isValid) {
            return Meteor.users.upsert({ _id: id }, { $set: user }, { validate: false });
        } 
            throw new Meteor.Error('User data is not valid');
    },
    removeUser: id => {
        check(id, String);
        return Meteor.users.remove(id);
    },
    inviteMember: (email, roles = ['member']) => {
      check(email, String);
      check(roles, Match.Optional(Array));
      const user = Meteor.users.findOne({ 'emails.address': email });
      const firstName = email && email.replace(/@.*$/, '');
      const profile = {
        name: { first: firstName, last: '' },
        email,
      };
      if (!user) {
        const userId = Accounts.createUser({ email, profile });
        Roles.addUsersToRoles(userId, roles);
        const newUser = Meteor.users.findOne(userId);
        Accounts.sendEnrollmentEmail(newUser);
        return newUser;
      }
      return user;
    }
});
