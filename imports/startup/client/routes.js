/* eslint-disable max-len */

import React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { Meteor } from 'meteor/meteor';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import MuiTheme from './MuiTheme';
import injectTapEventPlugin from 'react-tap-event-plugin';
import App from '../../ui/layouts/App.js';

import Documents from '../../ui/pages/Documents.js';
import NewDocument from '../../ui/pages/NewDocument.js';
import EditDocument from '../../ui/pages/EditDocument.js';
import ViewDocument from '../../ui/pages/ViewDocument.js';

import Index from '../../ui/pages/Index.js';
import Login from '../../ui/pages/Login.js';
import NotFound from '../../ui/pages/NotFound.js';
import RecoverPassword from '../../ui/pages/RecoverPassword.js';
import ResetPassword from '../../ui/pages/ResetPassword.js';
import Signup from '../../ui/pages/Signup.js';

import Reminders from '../../ui/pages/Reminders.js';

// New Routes
import Teams from '../../ui/pages/Teams.js';
import NewTeam from '../../ui/pages/NewTeam.js';
import EditTeam from '../../ui/pages/EditTeam.js';
import ViewTeam from '../../ui/pages/ViewTeam.js';

import Tasks from '../../ui/pages/Tasks.js';
import NewTask from '../../ui/pages/NewTask.js';
import EditTask from '../../ui/pages/EditTask.js';
import ViewTask from '../../ui/pages/ViewTask.js';

import EditProfile from '../../ui/pages/EditProfile.js';

injectTapEventPlugin();

const authenticate = (nextState, replace) => {
  if (!Meteor.loggingIn() && !Meteor.userId()) {
    replace({
      pathname: '/login',
      state: { nextPathname: nextState.location.pathname }
    });
  }
};

Meteor.startup(() => {
  render(
    <MuiThemeProvider muiTheme={MuiTheme}>
      <Router history={browserHistory}>
        <Route path='/' component={App}>
          <IndexRoute name='index' component={Index} onEnter={authenticate}/>

          <Route name='reminders' path='/reminders' component={Reminders} onEnter={authenticate} />

          <Route name='documents' path='/documents' component={Documents} onEnter={authenticate} />
          <Route name='newDocument' path='/documents/new' component={NewDocument} onEnter={authenticate} />
          <Route name='editDocument' path='/documents/:_id/edit' component={EditDocument} onEnter={authenticate} />
          <Route name='viewDocument' path='/documents/:_id' component={ViewDocument} onEnter={authenticate} />

          <Route name='teams' path='/teams' component={Teams} onEnter={authenticate} />
          <Route name='editTeam' path='/teams/:_id/edit' component={EditTeam} onEnter={authenticate} />
          <Route name='NewTeam' path='/teams/new' component={NewTeam} onEnter={authenticate} />
          <Route name='ViewTeam' path='/teams/:_id' component={ViewTeam} onEnter={authenticate} />

          <Route name='tasks' path='/tasks' component={Tasks} onEnter={authenticate} />
          <Route name='newTask' path='/tasks/new' component={NewTask} onEnter={authenticate} />
          <Route name='editTask' path='/tasks/:_id/edit' component={EditTask} onEnter={authenticate} />
          <Route name='viewTask' path='/tasks/:_id' component={ViewTask} onEnter={authenticate} />

          <Route name='editProfile' path='/profile/:_id' component={EditProfile} onEnter={authenticate} />

          <Route name='login' path='/login' component={Login} />
          <Route name='recover-password' path='/recover-password' component={RecoverPassword} />
          <Route name='reset-password' path='/reset-password/:token' component={ResetPassword} />
          <Route name='enroll-account' path='/enroll-account/:token' component={ResetPassword} />
          <Route name='signup' path='/signup' component={Signup} />
          <Route path='*' component={NotFound} />
        </Route>
      </Router>
    </MuiThemeProvider>,
    document.getElementById('react-root')
  );
});
