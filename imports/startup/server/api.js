import '../../api/documents/methods.js';
import '../../api/documents/server/publications.js';

import '../../api/customers/methods.js';
import '../../api/customers/server/publications.js';

import '../../api/facilities/methods.js';
import '../../api/facilities/server/publications.js';

import '../../api/devices/methods.js';
import '../../api/devices/server/publications.js';

import '../../api/testers/methods.js';
import '../../api/testers/server/publications.js';

import '../../api/company/methods.js';
import '../../api/company/server/publications.js';

import '../../api/testings/methods.js';
import '../../api/testings/server/publications.js';

// New methods
import '../../api/teams/methods.js';
import '../../api/teams/server/publications.js';

import '../../api/tasks/methods.js';
import '../../api/tasks/server/publications.js';

import '../../api/files/methods.js';
import '../../api/files/server/publications.js';

import '../../api/users/methods.js';
