import { Accounts } from 'meteor/accounts-base';
import { Meteor } from 'meteor/meteor';

const name = 'Wakflo';
const email = '<support@wakflo.com>';
const from = `${name} ${email}`;
const emailTemplates = Accounts.emailTemplates;

process.env.MAIL_URL = 'smtp://postmaster%40mailgun.wakflo.com:88a96c52f615969b6baf96009d3bd7cc-4836d8f5-d5396b13@smtp.mailgun.org:587';
Accounts.urls.enrollAccount = token => Meteor.absoluteUrl(`enroll-account/${token}`);
emailTemplates.siteName = name;
emailTemplates.from = from;

emailTemplates.enrollAccount.subject = () => `Invitation to join, ${name}`;

emailTemplates.resetPassword = {
  subject() {
    return `[${name}] Reset Your Password`;
  },
  text(user, url) {
    const userEmail = user.emails[0].address;
    const urlWithoutHash = url.replace('#/', '');

    return `A password reset has been requested for the account related to this
    address (${userEmail}). To reset the password, visit the following link:
    \n\n${urlWithoutHash}\n\n If you did not request this reset, please ignore
    this email. If you feel something is wrong, please contact our support team:
    ${email}.`;
  },
};
