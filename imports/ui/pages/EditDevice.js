import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import Paper from 'material-ui/Paper';
import { Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle } from 'material-ui/Toolbar';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';

import container from '../../modules/container';
import Devices from '../../api/devices/devices';
import DeviceEditor from '../components/DeviceEditor';
import FacilityEditor from '../components/FacilityEditor.js';
import NotFound from './NotFound';
import FacilityAutocomplete from '../components/FacilityAutocomplete';
import ActivityFeed from '../components/extras/justForShow/ActivityFeed';


const style = {
  height: 'auto',
  width: '100%',
  padding: 20,
  display: 'inline-block',
  marginTop: 10,
};

class EditDevice extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            selectedFacility: null
        };
        this.setSelectedFacility = this.setSelectedFacility.bind(this);
    }

    handleOpen() {
        this.setState({ open: true });
    }

    handleClose() {
        this.setState({ open: false });
    }

    getFacility() {
        return this.state.selectedFacility;
    }

    setSelectedFacility(facility, idx) {
        this.setState({ selectedFacility: facility });
    }

    render() {
        const { device } = this.props;

        const actions = [
            <FlatButton
                label="Cancel"
                primary
                onTouchTap={this.handleClose.bind(this)}
            />,
            <FlatButton
                label="Save"
                primary
                keyboardFocused
                onTouchTap={this.handleClose.bind(this)}
            />,
        ];

        return (
            device ? (
                <Row>
                    <Col xs={12} lg={8} lgOffset={2}>
                        <Paper zDepth={1}>
                            <Toolbar style={{ color: '#ffffff', backgroundColor: '#0052A5' }}>
                                <ToolbarGroup firstChild>
                                    <ToolbarTitle style={{ paddingLeft: 20, color: '#ffffff' }} text="Edit Device" />
                                </ToolbarGroup>
                                <ToolbarGroup>
                                    <ToolbarSeparator />
                                    <RaisedButton label="Email" primary={false} style={{ margin: 5 }} />
                                    <RaisedButton label="Print Invoice" primary={false} style={{ margin: 5 }} />
                                    <RaisedButton label="Do Something" primary={false} style={{ margin: 5 }} />
                                </ToolbarGroup>
                            </Toolbar>
                        </Paper>
                    </Col>
                    <Col xs={12} md={9} lg={6} lgOffset={2}>
                        <Paper style={style} zDepth={1}>
                            <Row>
                                <Col xs={12}>
                                    <h4 className="pull-left">Linked Facility</h4>
                                </Col>
                                <Col xs={12}>
                                    <p>Every Device needs to be linked to a facility</p>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs={12} sm={12} md={6}>
                                    <FacilityAutocomplete
                                        facilityId={device.facilityId}
                                        setFacility={this.setSelectedFacility}
                                    />
                                </Col>
                                <Col xs={1} sm={1} md={1}>
                                    <p>-OR-</p>
                                </Col>
                                <Col xs={12} sm={12} md={5}>
                                    <RaisedButton label="Create New Facility" primary onTouchTap={this.handleOpen.bind(this)} />
                                </Col>
                            </Row>
                        </Paper>
                        <Paper style={style} zDepth={1}>
                            <Row>
                                <Col xs={12}>
                                    <h4 className="pull-left">Device Information</h4>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs={12}>
                                    <DeviceEditor device={device} getFacility={this.getFacility.bind(this)} />
                                </Col>
                            </Row>
                        </Paper>
                    </Col>
                    <Col xs={12} md={3} lg={2} >
                        <Paper style={style} zDepth={1}>
                            <Row>
                                <Col xs={12}>
                                    <h4 className="pull-left">Activity Log</h4>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs={12}>
                                    <ActivityFeed />
                                </Col>
                            </Row>
                        </Paper>
                    </Col>
                    <Dialog
                        title="Create New Facility"
                        actions={actions}
                        modal={false}
                        open={this.state.open}
                        onRequestClose={this.handleClose}
                        autoScrollBodyContent
                    >
                            <Row>
                                <Col xs={12}>
                                    <FacilityEditor />
                                </Col>
                            </Row>
                        </Dialog>
                </Row>
            ) : <NotFound />);
    }
}

EditDevice.propTypes = {
    device: PropTypes.object,
};

export default container((props, onData) => {
    const deviceId = props.params._id;
    const subscription = Meteor.subscribe('devices.view', deviceId);

    if (subscription.ready()) {
        const device = Devices.findOne(deviceId);
        onData(null, { device });
    }
}, EditDevice);
