import React from 'react';
import { Link } from 'react-router';
import CompanyList from '../components/CompanyList';
import CompanyEditor from '../components/CompanyEditor';
import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';

const style = {
  height: 'auto',
  width: '100%',
  padding: 20,
  display: 'inline-block',
};

const Companies = () => (
  <Col xs={12} md={8} mdOffset={2}>
    <Paper style={style} zDepth={1}>
      <Row>
        <Col xs={12}>
          <h4 className="pull-left">Companies</h4>
        </Col>
      </Row>
      <Row>
      <Col xs={12}>
          <CompanyEditor />
        </Col>
        <Col xs={12}>
          <CompanyList />
        </Col>
      </Row>
    </Paper>
  </Col>
);

export default Companies;
