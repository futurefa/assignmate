import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { browserHistory } from 'react-router';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import IconButton from 'material-ui/IconButton';
import Paper from 'material-ui/Paper';
import { Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle } from 'material-ui/Toolbar';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import Toggle from 'material-ui/Toggle';
import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import { grey600 } from 'material-ui/styles/colors';

import Teams from '../../api/teams/teams';
import TeamEditor from '../components/TeamEditor';
import TasksList from '../components/TasksList';
import NotFound from './NotFound';
import container from '../../modules/container';

import ActivityFeed from '../components/extras/justForShow/ActivityFeed';

class EditTeam extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            selectedCustomer: null,
        };
        this.setSelectedCustomer = this.setSelectedCustomer.bind(this);
    }

    handleOpen() {
        this.setState({ open: true });
    }

    handleClose() {
        this.setState({ open: false });
    }

    getCustomer() {
        return this.state.selectedCustomer;
    }

    setSelectedCustomer(customer, idx) {
        this.setState({ selectedCustomer: customer });
    }

    render() {
        const { selectedCustomer } = this.state;
        const actions = [
            <FlatButton
                label="Cancel"
                primary
                onTouchTap={this.handleClose.bind(this)}
            />,
            <FlatButton
                label="Save"
                primary
                keyboardFocused
                onTouchTap={this.handleClose.bind(this)}
            />,
        ];

        const style = {
            height: 'auto',
            width: '100%',
            padding: 20,
            display: 'inline-block',
            marginTop: 10,
        };
        const { team, members } = this.props;
        return (
            team ? (
              <Row>
                <Col xs={12} lg={8} lgOffset={2}>
                  <Paper zDepth={1}>
                    <Toolbar style={{ color: '#ffffff', backgroundColor: grey600 }}>
                      <ToolbarGroup firstChild>
                        <IconButton onTouchTap={() => browserHistory.push('/teams')}>
                          <ArrowBack color='#ffffff' />
                        </IconButton>
                        <ToolbarTitle style={{ paddingLeft: 20, color: '#ffffff' }} text="Edit Team" />
                      </ToolbarGroup>
                      <ToolbarGroup>
                        <ToolbarSeparator />
                        <Toggle label="Edit" onTouchTap={() => browserHistory.push(`teams/${team._id}`)} style={{ width: 'initial', margin: 10 }} labelStyle={{ color: '#fff' }} />
                        <RaisedButton label="Email" primary={false} style={{ margin: 5 }} />
                        <RaisedButton label="Print Invoice" primary={false} style={{ margin: 5 }} />
                      </ToolbarGroup>
                    </Toolbar>
                  </Paper>
                </Col>
                <Col xs={12} md={9} lg={6} lgOffset={2}>
                  <Paper style={style} zDepth={1}>
                    <Row>
                      <Col xs={12}>
                        <h4 className="pull-left">Team Information</h4>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs={12}>
                        <TeamEditor team={team} getCustomer={this.getCustomer.bind(this)} members={members} />
                      </Col>
                    </Row>
                  </Paper>
                  <Paper style={style} zDepth={1}>
                    <Row>
                      <Col xs={12}>
                        <Toolbar style={{ backgroundColor: '#ffffff' }}>
                          <ToolbarGroup firstChild>
                            <ToolbarTitle style={{ paddingLeft: 20 }} text="Team Assignments" />
                          </ToolbarGroup>
                          <ToolbarGroup>
                            <ToolbarSeparator />
                            <RaisedButton label="Create" primary disabled />
                            <RaisedButton label="Connect" primary={false} disabled />
                          </ToolbarGroup>
                        </Toolbar>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs={12}>
                        <TasksList team={team} />
                      </Col>
                    </Row>
                  </Paper>
                </Col>
                <Col xs={12} md={3} lg={2} >
                  <Paper style={style} zDepth={1}>
                    <Row>
                      <Col xs={12}>
                        <h4 className="pull-left">Activity Log</h4>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs={12}>
                        <ActivityFeed />
                      </Col>
                    </Row>
                  </Paper>
                </Col>
              </Row>
        ) : <NotFound />);
    }
}

EditTeam.propTypes = {
    team: PropTypes.object,
    members: PropTypes.array,
};

export default container((props, onData) => {
    const teamId = props.params._id;
    const subscription = Meteor.subscribe('teams.view', teamId);
    const subscriptionMembers = Meteor.subscribe('teams.view.members', teamId);
    if (subscription.ready() && subscriptionMembers.ready()) {
        const team = Teams.findOne(teamId);
        const query = { _id: { $in: team && team.contacts && _.pluck(team.contacts, '_id') } };
        const members = Meteor.users.find(query).fetch();
        onData(null, { team, members });
    }
}, EditTeam);
