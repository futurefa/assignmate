import React from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import Company from '../../api/company/company';
import CompanyEditor from '../components/CompanyEditor';
import NotFound from './NotFound';
import container from '../../modules/container';

import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import ActivityFeed from '../components/extras/justForShow/ActivityFeed';
import Paper from 'material-ui/Paper';
import { Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle } from 'material-ui/Toolbar';
import RaisedButton from 'material-ui/RaisedButton';


const style = {
  height: 'auto',
  width: '100%',
  padding: 20,
  display: 'inline-block',
  marginTop: 10,
};

const EditCompany = ({ company }) => (company ? (
    <Row>
      <Col xs={12} lg={8} lgOffset={2}>
        <Paper zDepth={1}>
          <Toolbar style={{ color: '#ffffff', backgroundColor: '#0052A5' }}>
            <ToolbarGroup firstChild>
              <ToolbarTitle style={{ paddingLeft: 20, color: '#ffffff' }} text="Edit company" />
            </ToolbarGroup>
            <ToolbarGroup>
              <ToolbarSeparator />
              <RaisedButton label="Email" primary={false} style={{ margin: 5 }} />
              <RaisedButton label="Print Invoice" primary={false} style={{ margin: 5 }} />
              <RaisedButton label="Do Something" primary={false} style={{ margin: 5 }} />
            </ToolbarGroup>
          </Toolbar>
        </Paper>
      </Col>
      <Col xs={12} md={9} lg={6} lgOffset={2}>
        <Paper style={style} zDepth={1}>
          <Row>
            <Col xs={12}>
              <h4 className="pull-left">Company Information</h4>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <CompanyEditor company={company} />
            </Col>
          </Row>
        </Paper>
      </Col>
      <Col xs={12} md={3} lg={2} >
        <Paper style={style} zDepth={1}>
          <Row>
            <Col xs={12}>
              <h4 className="pull-left">Activity Log</h4>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <ActivityFeed />
            </Col>
          </Row>
        </Paper>
      </Col>
    </Row>
) : <NotFound />);

EditCompany.propTypes = {
    company: PropTypes.object,
};

export default container((props, onData) => {
    const companyId = props.params._id;
    const subscription = Meteor.subscribe('company.view', companyId);

    if (subscription.ready()) {
        const company = Company.findOne(companyId);
        onData(null, { company });
    }
}, EditCompany);
