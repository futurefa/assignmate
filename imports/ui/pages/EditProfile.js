import React from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import Documents from '../../api/documents/documents';
import DocumentEditor from '../components/DocumentEditor';
import NotFound from './NotFound';
import container from '../../modules/container';
import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import Paper from 'material-ui/Paper';


const style = {
  height: 'auto',
  width: '100%',
  padding: 20,
  display: 'inline-block',
};

const EditProfile = ({ user }) => (user ? (
  <Col xs={12} md={8} mdOffset={2}>
    <Paper style={style} zDepth={1}>
      <Row>
        <Col xs={12}>
          <h4 className="pull-left">Editing "{ user.profile && user.profile.name && user.profile.name.first }"</h4>
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <DocumentEditor doc={user} />
        </Col>
      </Row>
    </Paper>
  </Col>
) : <NotFound />);

EditProfile.propTypes = {
  user: PropTypes.object,
};

export default container((props, onData) => {
  const userId = props.params._id;
  const subscription = Meteor.subscribe('documents.view', userId);

  if (subscription.ready()) {
    const user = Meteor.users.findOne(userId);
    onData(null, { user });
  }
}, EditProfile);
