import React from 'react';
import { Link, browserHistory } from 'react-router';
import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import TasksList from '../components/TasksList';

const style = {
  height: 'auto',
  width: '100%',
  padding: 20,
  display: 'inline-block',
};

const fabStyle = {
    position: 'fixed',
    zIndex: 4500,
    right: 20,
    bottom: 20,
    overflow: 'visible',
    transition: 'opacity .2s',
    textAlign: 'center',
};

const Tasks = () => (
  <div>
    <Col xs={12} md={8} mdOffset={2}>
      <Paper style={style} zDepth={1}>
        <Row>
          <Col xs={12}>
            <h4 className="pull-left">Tasks</h4>
          </Col>
        </Row>
        <Row>
          <Col xs={12}>
            <TasksList />
          </Col>
        </Row>
      </Paper>
    </Col>
    <FloatingActionButton
      onTouchTap={() => (browserHistory.push('/tasks/new'))}
      className='pull-right'
      primary
      style={fabStyle}
    >
        <ContentAdd />
    </FloatingActionButton>
  </div>
);

export default Tasks;
