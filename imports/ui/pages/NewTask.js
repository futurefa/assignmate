import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import TaskEditor from '../components/TaskEditor.js';
import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import Paper from 'material-ui/Paper';
import { Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle } from 'material-ui/Toolbar';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import Dialog from 'material-ui/Dialog';
import AutoComplete from 'material-ui/AutoComplete';
import MenuItem from 'material-ui/MenuItem';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import { grey600 } from 'material-ui/styles/colors';
import container from '../../modules/container';

import TeamEditor from '../components/TeamEditor.js';
import ActivityFeed from '../components/extras/justForShow/ActivityFeed';
import TeamAutocomplete from '../components/TeamAutocomplete';
import { Alert } from 'react-bootstrap';


class NewTask extends Component {

    render() {
        const style = {
            height: 'auto',
            width: '100%',
            padding: 20,
            display: 'inline-block',
            marginTop: 10,
        };

        return (
            <Row>
              <Col xs={12} lg={8} lgOffset={2}>
                <Paper zDepth={1}>
                  <Toolbar style={{ color: '#ffffff', backgroundColor: grey600 }}>
                    <ToolbarGroup firstChild>
                      <IconButton onTouchTap={() => browserHistory.push('/tasks')}>
                        <ArrowBack color='#ffffff' />
                      </IconButton>
                      <ToolbarTitle style={{ paddingLeft: 20, color: '#ffffff' }} text="New Task" />
                    </ToolbarGroup>
                    <ToolbarGroup>
                      <ToolbarSeparator />
                      <RaisedButton label="Email" primary={false} style={{ margin: 5 }} />
                      <RaisedButton label="Print" primary={false} style={{ margin: 5 }} />
                      <RaisedButton label="Do" primary={false} style={{ margin: 5 }} />
                    </ToolbarGroup>
                  </Toolbar>
                </Paper>
              </Col>
              <Col xs={12} md={9} lg={6} lgOffset={2}>
                    <Paper style={style} zDepth={1}>
                        <Row>
                            <Col xs={12}>
                                <h4 className="pull-left">Task Information</h4>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12}>
                                <TaskEditor />
                            </Col>
                        </Row>
                    </Paper>
                </Col>
                <Col xs={12} md={3} lg={2} >
                    <Paper style={style} zDepth={1}>
                        <Row>
                            <Col xs={12}>
                                <h4 className="pull-left">Activity Log</h4>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12}>
                                <ActivityFeed />
                            </Col>
                        </Row>
                    </Paper>
                </Col>
            </Row>
        );
    }
}

export default NewTask;
