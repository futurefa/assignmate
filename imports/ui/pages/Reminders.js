import React from 'react';
import { Link } from 'react-router';
import DevicesDueForTestList from '../components/DevicesDueForTestList';
import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import Paper from 'material-ui/Paper';

const style = {
  height: 'auto',
  width: '100%',
  padding: 20,
  display: 'inline-block',
};

const Reminders = () => (
  <Col xs={12} md={8} mdOffset={2}>
    <Paper style={style} zDepth={1}>
      <Row>
        <Col xs={12}>
          <h4 className="pull-left">Devices Due for Tests</h4>
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <DevicesDueForTestList />
        </Col>
      </Row>
    </Paper>
  </Col>
);

export default Reminders;
