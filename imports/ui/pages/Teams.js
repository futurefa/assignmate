import React from 'react';
import { browserHistory } from 'react-router';
import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';

import TeamEditor from '../components/TeamEditor.js';
import TeamsList from '../components/TeamsList';

export default class Teams extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }

  handleOpen() {
      this.setState({ open: true });
  }

  handleClose() {
      this.setState({ open: false });
  }

  render() {
    const style = {
      height: 'auto',
      width: '100%',
      padding: 20,
      display: 'inline-block'
    };

    const fabStyle = {
      position: 'fixed',
      zIndex: 4500,
      right: 20,
      bottom: 20,
      overflow: 'visible',
      transition: 'opacity .2s',
      textAlign: 'center',
    };

    const actions = [
        <FlatButton
            label="Cancel"
            primary
            onTouchTap={this.handleClose.bind(this)}
        />,
        <FlatButton
            label="Save"
            primary
            keyboardFocused
            onTouchTap={this.handleClose.bind(this)}
        />,
    ];


    return (
      <div>
        <Col xs={12} md={8} mdOffset={2}>
          <Paper style={style} zDepth={1}>
            <Row>
              <Col xs={12}>
                <TeamsList />
              </Col>
            </Row>
          </Paper>
          <Dialog
            title="Create Team"
            actions={actions}
            modal={false}
            open={this.state.open || false}
            onRequestClose={this.handleClose}
            autoScrollBodyContent
          >
                <Row>
                    <Col xs={12}>
                        <TeamEditor />
                    </Col>
                </Row>
            </Dialog>
        </Col>
        <FloatingActionButton
          onTouchTap={this.handleOpen.bind(this)}
          className='pull-right'
          primary
          style={fabStyle}
        >
            <ContentAdd />
        </FloatingActionButton>
      </div>
    );
  }
}
