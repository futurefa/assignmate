import React from 'react';
import { Link } from 'react-router';
import TestersList from '../components/TestersList';
import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';

const style = {
  height: 'auto',
  width: '100%',
  padding: 20,
  display: 'inline-block',
};

const Testers = () => (
  <Col xs={12} md={8} mdOffset={2}>
    <Paper style={style} zDepth={1}>
      <Row>
        <Col xs={12}>
          <h4 className="pull-left">Testers</h4>
          <Link to="/testers/new">
            <RaisedButton
              onTouchTap={() => (browserHistory.push('/testers'))}
              className="pull-right"
              primary
              label="New"
            />
          </Link>
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <TestersList />
        </Col>
      </Row>
    </Paper>
  </Col>
);

export default Testers;
