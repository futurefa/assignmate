import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Meteor } from 'meteor/meteor';
import { Bert } from 'meteor/themeteorchef:bert';
import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import Paper from 'material-ui/Paper';
import { Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle } from 'material-ui/Toolbar';
import Toggle from 'material-ui/Toggle';
import { Tab, Tabs } from 'react-bootstrap';
import IconButton from 'material-ui/IconButton';
import ActionDelete from 'material-ui/svg-icons/action/delete';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import Linkify from 'react-linkify';
import { grey600 } from 'material-ui/styles/colors';

import Tasks from '../../api/tasks/tasks';
import Teams from '../../api/teams/teams';
import { removeTask } from '../../api/tasks/methods';
import NotFound from './NotFound';
import container from '../../modules/container';
import UploadsList from '../components/UploadsList';
import ContactList from '../components/ContactList';
import ActivityFeed from '../components/extras/justForShow/ActivityFeed';

const handleEdit = (_id) => {
  browserHistory.push(`/tasks/${_id}/edit`);
};

const handleRemove = (_id) => {
  if (confirm('Are you sure? This is permanent!')) {
    removeTask.call({ _id }, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert('Task deleted!', 'success');
        browserHistory.push('/tasks');
      }
    });
  }
};

const style = {
    height: 'auto',
    width: '100%',
    padding: 20,
    display: 'inline-block',
    marginTop: 10,
};

const ViewTask = ({ task, team }) => task ? (
    <Row>
      <Col xs={12} lg={8} lgOffset={2}>
        <Paper zDepth={1}>
          <Toolbar style={{ color: '#ffffff', backgroundColor: grey600 }}>
            <ToolbarGroup firstChild>
              <IconButton onTouchTap={() => browserHistory.push('/tasks')}>
                <ArrowBack color='#ffffff' />
              </IconButton>
              <ToolbarTitle style={{ paddingLeft: 20, color: '#ffffff' }} text="View Task" />
            </ToolbarGroup>
            <ToolbarGroup>
              <ToolbarSeparator />
              <Toggle 
                label="Edit" 
                onToggle={(e, checked) => checked ? handleEdit(task._id) : null} 
                style={{ width: 'initial', margin: 10 }} 
                labelStyle={{ color: '#fff' }} 
              />
              
              {/*<RaisedButton onClick={ () => handleEdit(task._id) }>Edit</RaisedButton>*/}
              <IconButton onTouchTap={() => handleRemove(task._id)}>
                <ActionDelete color='#ffffff' />
              </IconButton>
{/*              <RaisedButton onClick={ () => handleRemove(task._id) } className="text-danger">Delete</RaisedButton>
*/}                                         </ToolbarGroup>
          </Toolbar>
        </Paper>
      </Col>
      <Col xs={12} md={9} lg={6} lgOffset={2}>
        <Paper style={style} zDepth={1}>
          <Row>
            <Col xs={12}>
              <h2 className="pull-left">{ task && task.title }</h2>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <Tabs
                id="controlled-tab-example"
              >
                <Tab eventKey={1} title="Overview">
                  <Paper style={style} zDepth={1}>
                    <div style={{ whiteSpace: 'pre-wrap' }}>
                    <Linkify options={{ ignoreTags: ['style'] }} properties={{ target: '_blank' }}>
                      { task && task.body }
                    </Linkify>
                    </div>
                  </Paper>
                </Tab>
                <Tab eventKey={2} title="Attachments">
                  <Paper style={style} zDepth={1}>
                    <UploadsList item={task} />
                  </Paper>
                </Tab>
                <Tab eventKey={3} title="Members">
                  <Paper style={style} zDepth={1}>
                    <ContactList item={team} />
                  </Paper>
                </Tab>
                <Tab eventKey={4} title="Submissions">
                  <Paper style={style} zDepth={1}>
                    List of Submitted assignments
                  </Paper>
                </Tab>
              </Tabs>
            </Col>
          </Row>
        </Paper>
      </Col>
      <Col xs={12} md={3} lg={2} >
        <Paper style={style} zDepth={1}>
          <Row>
            <Col xs={12}>
              <h4 className="pull-left">Activity Log</h4>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <ActivityFeed />
            </Col>
          </Row>
        </Paper>
      </Col>
    </Row>
  ) : <NotFound />;

ViewTask.propTypes = {
  task: PropTypes.object,
  team: PropTypes.object,
};

export default container((props, onData) => {
  const taskId = props.params._id;
  const subscription = Meteor.subscribe('tasks.view', taskId);
  const teamSubscription = Meteor.subscribe('tasks.view.team', taskId);

  if (subscription.ready() && teamSubscription.ready()) {
    const task = Tasks.findOne(taskId);
    const team = Teams.findOne(task.team && task.team._id);
    onData(null, { task, team });
  }
}, ViewTask);
