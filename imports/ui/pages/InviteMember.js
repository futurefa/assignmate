import React from 'react';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';
import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import _ from 'lodash';


export default class InviteMember extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
        };
    }
    
    handleSendInvite() {
        const member = {};
        member.email = this.refs.email.getValue();
        if (this.props.onChange && member.email) {
            this.props.onChange(member);
            this.cleanInput();
        }
    }

    handleInputChange(event) {
        this.setState({
            email: event.target.value
        });
    }

    cleanInput() {
        this.setState({
            email: ''
        });
    }

    render() {
        const self = this;

        return (
            <Row>
                <Col xs={12} md={12} sm={12}>
                    <TextField
                        ref="email"
                        name="email"
                        floatingLabelText="email"
                        fullWidth
                        hintText="email"
                        value={this.state.email}
                        onChange={this.handleInputChange.bind(this)}
                    />
                    <FlatButton
                    label='Add'
                    primary
                    keyboardFocused
                    onTouchTap={this.handleSendInvite.bind(this)}
                  />
                </Col>
            </Row>
        );
    }
}

InviteMember.propTypes = {
    team: PropTypes.object,
    onChange: PropTypes.func,
};
