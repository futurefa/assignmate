import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { browserHistory } from 'react-router';
import Paper from 'material-ui/Paper';
import { Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle } from 'material-ui/Toolbar';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import Toggle from 'material-ui/Toggle';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import IconButton from 'material-ui/IconButton';
import { grey600 } from 'material-ui/styles/colors';


import Tasks from '../../api/tasks/tasks';
import TaskEditor from '../components/TaskEditor';
import NotFound from './NotFound';
import container from '../../modules/container';

import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import ActivityFeed from '../components/extras/justForShow/ActivityFeed';
import TeamsList from '../components/TeamsList';

class EditTask extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            selectedTeam: null,
        };
        this.setSelectedTeam = this.setSelectedTeam.bind(this);
    }

    handleOpen() {
        this.setState({ open: true });
    }

    handleClose() {
        this.setState({ open: false });
    }

    getTeam() {
        return this.state.selectedTeam;
    }

    setSelectedTeam(team, idx) {
        this.setState({ selectedTeam: team });
    }

    render() {
        const { selectedTeam } = this.state;
        const actions = [
            <FlatButton
                label="Cancel"
                primary
                onTouchTap={this.handleClose.bind(this)}
            />,
            <FlatButton
                label="Save"
                primary
                keyboardFocused
                onTouchTap={this.handleClose.bind(this)}
            />,
        ];

        const style = {
            height: 'auto',
            width: '100%',
            padding: 20,
            display: 'inline-block',
            marginTop: 10,
        };
        const { task } = this.props;
        return (
            task ? (
              <Row>
                <Col xs={12} lg={8} lgOffset={2}>
                  <Paper zDepth={1}>
                    <Toolbar style={{ color: '#ffffff', backgroundColor: grey600 }}>
                      <ToolbarGroup firstChild>
                        <IconButton onTouchTap={() => browserHistory.push('/tasks')}>
                          <ArrowBack color='#ffffff' />
                        </IconButton>
                        <ToolbarTitle style={{ paddingLeft: 20, color: '#ffffff' }} text="Edit Task" />
                      </ToolbarGroup>
                      <ToolbarGroup>
                        <ToolbarSeparator />
                        <Toggle 
                          label="Edit" 
                          style={{ width: 'initial', margin: 10 }} 
                          labelStyle={{ color: '#fff' }} 
                          defaultToggled
                          onToggle={(e, checked) => checked ? browserHistory.push(`/tasks/${task._id}/edit`) : browserHistory.push(`/tasks/${task._id}`)} 
                        />
                        {/*<RaisedButton label="Email" primary={false} style={{margin: 5}} />
                        <RaisedButton label="Print" primary={false} style={{margin: 5}} />*/}
                      </ToolbarGroup>
                    </Toolbar>
                  </Paper>
                </Col>
                <Col xs={12} md={9} lg={6} lgOffset={2}>
                  
                  <Paper style={style} zDepth={1}>
                    <Row>
                      <Col xs={12}>
                        <h4 className="pull-left">Task Information</h4>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs={12}>
                        <TaskEditor task={task} getTeam={this.getTeam.bind(this)} />
                      </Col>
                    </Row>
                  </Paper>
                </Col>
                <Col xs={12} md={3} lg={2} >
                  <Paper style={style} zDepth={1}>
                    <Row>
                      <Col xs={12}>
                        <h4 className="pull-left">Activity Log</h4>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs={12}>
                        <ActivityFeed />
                      </Col>
                    </Row>
                  </Paper>
                </Col>
              </Row>
        ) : <NotFound />);
    }
}

EditTask.propTypes = {
    task: PropTypes.object,
};

export default container((props, onData) => {
    const taskId = props.params._id;
    const subscription = Meteor.subscribe('tasks.view', taskId);
    if (subscription.ready()) {
        const task = Tasks.findOne(taskId);
        onData(null, { task });
    }
}, EditTask);
