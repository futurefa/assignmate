import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Meteor } from 'meteor/meteor';
import { Bert } from 'meteor/themeteorchef:bert';
import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import Paper from 'material-ui/Paper';
import { Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle } from 'material-ui/Toolbar';
import Toggle from 'material-ui/Toggle';
import { Tab, Tabs } from 'react-bootstrap';
import IconButton from 'material-ui/IconButton';
import ActionDelete from 'material-ui/svg-icons/action/delete';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import Linkify from 'react-linkify';
import { grey600 } from 'material-ui/styles/colors';

import Teams from '../../api/teams/teams';
import { removeTeam } from '../../api/teams/methods';
import NotFound from './NotFound';
import container from '../../modules/container';
import ContactList from '../components/ContactList';
import ActivityFeed from '../components/extras/justForShow/ActivityFeed';

const handleEdit = (_id) => {
  browserHistory.push(`/teams/${_id}/edit`);
};

const handleRemove = (_id) => {
  if (confirm('Are you sure? This is permanent!')) {
    removeTeam.call({ _id }, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert('Team deleted!', 'success');
        browserHistory.push('/teams');
      }
    });
  }
};

const style = {
    height: 'auto',
    width: '100%',
    padding: 20,
    display: 'inline-block',
    marginTop: 10,
};

const ViewTeam = ({ team }) => team ? (
    <Row style={style}>
      <Col xs={12} lg={8} lgOffset={2}>
        <Paper zDepth={1}>
          <Toolbar style={{ color: '#ffffff', backgroundColor: grey600 }}>
            <ToolbarGroup firstChild>
              <IconButton onTouchTap={() => browserHistory.push('/teams')}>
                <ArrowBack color='#ffffff' />
              </IconButton>
              <ToolbarTitle style={{ paddingLeft: 20, color: '#ffffff' }} text="View Team" />
            </ToolbarGroup>
            <ToolbarGroup>
              <ToolbarSeparator />
              <Toggle 
                label="Edit" 
                onToggle={(e, checked) => checked ? handleEdit(team._id) : null} 
                style={{ width: 'initial', margin: 10 }} 
                labelStyle={{ color: '#fff' }} 
              />
              
              {/*<RaisedButton onClick={ () => handleEdit(team._id) }>Edit</RaisedButton>*/}
              <IconButton onTouchTap={() => handleRemove(team._id)}>
                <ActionDelete color='#ffffff' />
              </IconButton>                                         </ToolbarGroup>
          </Toolbar>
        </Paper>
      </Col>
      <Col xs={12} md={9} lg={6} lgOffset={2}>
        <Paper style={style} zDepth={1}>
          <Row>
            <Col xs={12}>
              <h2 className="pull-left">{ team && team.name }</h2>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <Tabs
                id="controlled-tab-example"
              >
                <Tab eventKey={1} title="Updates">
                  <Paper style={style} zDepth={1}>
                    <div style={{ whiteSpace: 'pre-wrap' }}>
                    <Linkify options={{ ignoreTags: ['style'] }} properties={{ target: '_blank' }}>
                      <ActivityFeed />
                    </Linkify>
                    </div>
                  </Paper>
                </Tab>
                <Tab eventKey={2} title="Members">
                  <Paper style={style} zDepth={1}>
                    <ContactList item={team} />
                  </Paper>
                </Tab>
                <Tab eventKey={3} title="Notifications">
                  <Paper style={style} zDepth={1}>
                    <ActivityFeed />
                  </Paper>
                </Tab>
              </Tabs>
            </Col>
          </Row>
        </Paper>
      </Col>
      <Col xs={12} md={3} lg={2} >
        <Paper style={style} zDepth={1}>
          <Row>
            <Col xs={12}>
              <h4 className="pull-left">Activity Log</h4>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <ActivityFeed />
            </Col>
          </Row>
        </Paper>
      </Col>
    </Row>
  ) : <NotFound />;

ViewTeam.propTypes = {
  team: PropTypes.object,
};

export default container((props, onData) => {
  const teamId = props.params._id;
  const subscription = Meteor.subscribe('teams.view', teamId);

  if (subscription.ready()) {
    const team = Teams.findOne(teamId);
    onData(null, { team });
  }
}, ViewTeam);
