import React from 'react';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';
import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Teams from '../../api/teams/teams';

import _ from 'lodash';


export default class ContactEditor extends React.Component {
    constructor(props) {
        super(props);
        this.handleInput = _.debounce(this.handleInput.bind(this), 2500);
        this.state = {
            _id: this.props.contact ? this.props.contact._id : '',
            active: this.props.contact ? this.props.contact.active : false
        };
    }

    getValues() {
        const values = {};
        const booleanValues = ['active'];
        const dropDownValues = [];
        for (const key in this.refs) {
            if (key) {
               values[key] = this.refs[key].getValue(); 
            }
        }
        values._id = this.state._id;
        _.each(booleanValues, (booleanValue) => { values[booleanValue] = this.state[booleanValue]; });
        _.each(dropDownValues, (dropDownValue) => { values[dropDownValue] = this.state[dropDownValue]; });
        return values;
    }

    handleCheckBoxInput(name, e, value) {
        const v = {};
        v[name] = value;
        this.setState(v, () => { this.handleInput(e); });
    }

    handleSelectInput(name, e, key, payload) {
        const v = {};
        v[name] = payload;
        this.setState(v, () => { this.handleInput(e); });
    }

    handleInput(e) {
        if (this.props && this.props.setContact) {
            this.props.setContact(this.getValues());
        }
    }

    render() {
        const { contact } = this.props;
        const styles = {
          block: {
            maxWidth: 250,
          },
          checkbox: {
            marginBottom: 16,
          },
        };

        return (
            <Row>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="contactName"
                        floatingLabelText="Contact name"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={contact && contact.contactName}
                        hintText="Contact Name"
                    />
                </Col>
                <Col xs={12} md={2} sm={6}>
                    <TextField
                        ref="email"
                        floatingLabelText="Email"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={contact && contact.email}
                        hintText="Email"
                    />
                </Col>
                <Col xs={12} md={2} sm={6}>
                    <TextField
                        ref="phone"
                        floatingLabelText="Phone"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={contact && contact.phone}
                        hintText="Phone"
                    />
                </Col>
                <Col xs={12} md={12} sm={12}>
                    <TextField
                        ref="comments"
                        fullWidth
                        multiLine
                        onChange={this.handleInput}
                        rows={2}
                        floatingLabelText="Comment"
                        defaultValue={contact && contact.comments}
                        hintText="comment"
                    />
                </Col>
                <Col xs={12} md={12} sm={12}>
                    <Checkbox
                        label="Active"
                        onCheck={this.handleCheckBoxInput.bind(this, 'active')}
                        defaultChecked={contact && contact.active}
                        hintText="Active"
                    />
                </Col>
            </Row>
        );
    }
}

ContactEditor.propTypes = {
  contact: PropTypes.object,
};
