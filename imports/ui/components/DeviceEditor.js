
import React from 'react';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';
import TestEditor from './TestEditor';
import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import DatePicker from 'material-ui/DatePicker';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';

import Devices from '../../api/devices/devices';
import Testings from '../../api/testings/testings';
import _ from 'lodash';

const moment = require('moment');


export default class DeviceEditor extends React.Component {
    constructor(props) {
        super(props);
        this.handleInput = _.debounce(this.handleInput.bind(this), 2500);
        this.state = {
            open: false,
            deviceId: this.props.device ? this.props.device._id : '',
            installDate: this.props.device ? this.props.device.installDate : null,
            nextTestDue: this.props.device ? this.props.device.nextTestDue : null,
            testMonth: this.props.device ? this.props.device.testMonth : null
        };
    }

    handleOpen() {
        this.setState({ open: true });
    }

    handleClose() {
        this.setState({ open: false });
    }


    getValues() {
        const values = {};
        const booleanValues = ['active'];
        const dateValues = ['installDate', 'testMonth', 'nextTestDue'];
        const dropDownValues = ['protectionType', 'waterServiceType', 'orientationConfig'];
        for (const key in this.refs) {
            values[key] = this.refs[key].getValue();
        }
        _.each(dateValues, (dateValue) => { values[dateValue] = new Date(this.state[dateValue]); });
        _.each(booleanValues, (booleanValue) => { values[booleanValue] = this.state[booleanValue]; });
        _.each(dropDownValues, (dropDownValue) => { values[dropDownValue] = this.state[dropDownValue]; });
        return values;
    }

    handleDateInput(name, e, date) {
        v = {};
        v[name] = moment(date).endOf('day').toDate();
        this.setState(v, () => { this.saveDevice(); });
    }

    handleCheckBoxInput(name, e, value) {
        v = {};
        v[name] = value;
        this.setState(v, () => { this.saveDevice(); });
    }

    handleSelectInput(name, e, key, payload) {
        v = {};
        v[name] = payload;
        this.setState(v, () => { this.saveDevice(); });
    }

    handleInput(e) {
        e.preventDefault;
        this.saveDevice();
    }

    saveDevice() {
        const device = this.getValues();
        const { deviceId } = this.state;
        device.facilityId = this.props.getFacility() ? this.props.getFacility()._id : (this.props.device ? this.props.device.facilityId : null);
        Meteor.call('saveDevice', deviceId, device, (err, res) => {
            if (!err) {
              Bert.alert({
                  title: 'All Changes Saved',
                  type: 'success',
                  style: 'growl-top-right',
                  icon: 'fa-check'
              });
              if (!deviceId) { this.setState({ deviceId: res.insertedId }); }
            } else {
                console.log(err);
            }
        });
    }


    render() {
        const { device } = this.props;
        const actions = [
            <FlatButton
                label="Cancel"
                primary
                onTouchTap={this.handleClose.bind(this)}
            />,
            <FlatButton
                label="Save"
                primary
                keyboardFocused
                onTouchTap={this.handleClose.bind(this)}
            />,
        ];
        const styles = {
          block: {
            maxWidth: 250,
          },
          checkbox: {
            marginBottom: 16,
          },
        };

        return (
            <Row>
                {this.state.deviceId ?
                    <Col xs={12} sm={12} md={12}>
                            <RaisedButton label="Add Test" primary onTouchTap={this.handleOpen.bind(this)} />
                    </Col>
                    : null
                }
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="deviceName"
                        floatingLabelText="Device Name"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={device && device.deviceName}
                        hintText="Device Name"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="serialNumber"
                        floatingLabelText="Serial Number"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={device && device.serialNumber}
                        hintText="Serial Number"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="make"
                        floatingLabelText="Make"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={device && device.make}
                        hintText="Make"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="model"
                        floatingLabelText="Model"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={device && device.model}
                        hintText="Model"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="size"
                        floatingLabelText="Size"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={device && device.size}
                        hintText="Size"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <SelectField
                        fullWidth
                        floatingLabelText="Orientation Config"
                        onChange={this.handleSelectInput.bind(this, 'orientationConfig')}
                        value={device && device.orientationConfig}
                        children={
                            Devices.schema.getAllowedValuesForKey('orientationConfig').map(item => <MenuItem value={item} primaryText={item} />)
                        }
                        hintText="Orientation Config"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="type"
                        floatingLabelText="Type"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={device && device.type}
                        hintText="Type"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="deviceLocation"
                        floatingLabelText="Device Location"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={device && device.deviceLocation}
                        hintText="Device Location"
                    />
                </Col>
                <Col xs={12} md={4} sm={6}>
                        <SelectField
                        fullWidth
                        floatingLabelText="Water Service Type"
                        onChange={this.handleSelectInput.bind(this, 'waterServiceType')}
                        value={device && device.waterServiceType}
                        children={
                            Devices.schema.getAllowedValuesForKey('waterServiceType').map(item => <MenuItem value={item} primaryText={item} />)
                        }
                        hintText="Water Service Type"
                        />
                </Col>
                <Col xs={12} md={2} sm={6}>
                    <TextField
                        ref="hazardType"
                        floatingLabelText="Hazard Type"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={device && device.hazardType}
                        hintText="Hazard Type"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="waterAgency"
                        floatingLabelText="Water Agency"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={device && device.waterAgency}
                        hintText="Water Agency"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <SelectField
                        fullWidth
                        floatingLabelText="Protection Type"
                        onChange={this.handleSelectInput.bind(this, 'protectionType')}
                        value={device && device.protectionType}
                        children={
                            Devices.schema.getAllowedValuesForKey('protectionType').map(item => <MenuItem value={item} primaryText={item} />)
                        }
                        hintText="Protection Type"
                    />
                </Col>
                <Col xs={12} md={2} sm={6}>
                        <Checkbox
                        label="Active"
                        onCheck={this.handleCheckBoxInput.bind(this, 'active')}
                        defaultChecked={device && device.active}
                        />
                </Col>
                <Col xs={12} md={2} sm={6}>
                    <TextField
                        ref="hazardLevel"
                        floatingLabelText="Hazard Level"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={device && device.hazardLevel}
                        hintText="Hazard Level"
                    />
                </Col>
                <Col xs={12} md={2} sm={6}>
                    <DatePicker
                        name="installDate"
                        floatingLabelText="Install Date"
                        onChange={this.handleDateInput.bind(this, 'installDate')}
                        value={this.state.installDate}
                        fullWidth
                        hintText="Install Date"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="testCycle"
                        fullWidth
                        name="testCycle"
                        floatingLabelText="Test Cycle"
                        onChange={this.handleInput}
                        defaultValue={device && device.testCycle}
                        hintText="testCycle"
                    />
                </Col>
                <Col xs={12} md={2} sm={3}>
                    <DatePicker
                            name="testMonth"
                            floatingLabelText="Test Month"
                            onChange={this.handleDateInput.bind(this, 'testMonth')}
                            value={this.state.testMonth}
                            hintText="Test Month"
                    />
                </Col>
                <Col xs={12} md={3} sm={4}>
                    <DatePicker
                        floatingLabelText="Next Test Due"
                        onChange={this.handleDateInput.bind(this, 'nextTestDue')}
                        value={this.state.nextTestDue}
                        fullWidth
                        hintText="Next Test Due"
                    />
                </Col>
                <Dialog
                    title="Add New Test"
                    actions={actions}
                    modal={false}
                    open={this.state.open}
                    onRequestClose={this.handleClose}
                    autoScrollBodyContent
                >
                        <Row>
                            <Col xs={12}>
                                <TestEditor device={device} />
                            </Col>
                        </Row>
                    </Dialog>
            </Row>
        );
    }
}

DeviceEditor.propTypes = {
  device: PropTypes.object,
};
