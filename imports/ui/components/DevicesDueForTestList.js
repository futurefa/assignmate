import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Button, ButtonGroup, Alert } from 'react-bootstrap';
import { Meteor } from 'meteor/meteor';
import Devices from '../../api/devices/devices';
import Facilities from '../../api/facilities/facilities';
import Customers from '../../api/customers/customers';
import { removeDevice } from '../../api/devices/methods';
import container from '../../modules/container';
import Griddle from 'griddle-react';
import RaisedButton from 'material-ui/RaisedButton';

const moment = require('moment');

const style = {
    marginRight: 10
};

const actionButtons = React.createClass({
  render() {
    return (
        <div>
            <ButtonGroup>
                <Button bsSize="xsmall" onClick={() => browserHistory.push(`/devices/${this.props.rowData._id}`)}>Edit</Button>
                <Button bsSize="xsmall" onClick={() => handleRemove(this.props.rowData._id)} bsStyle="danger">Delete</Button>
            </ButtonGroup>
        </div>
    );
  }
});

const nextTestDueComponent = React.createClass({
    render() {
        const date = moment(this.props.rowData.nextTestDue).format('YYYY-MM-DD');
        return (
            <div>
                {date}
            </div>
        );
    }
});

const columnList = [
    'facilityName', 'customerName', 'make', 'size',
    'serialNumber', 'deviceLocation', 'active', 'nextTestDue',
    'actions'
];
const columnMetadata = [
    { columnName: 'facilityName', order: '1', displayName: 'Facility Name', cssClassName: 'col-xs-2' },
    { columnName: 'customerName', order: '2', displayName: 'Customer Name', cssClassName: 'col-xs-2' },
    { columnName: 'make', order: '3', displayName: 'Make', cssClassName: 'col-xs-2' },
    { columnName: 'size', order: '5', displayName: 'Size', cssClassName: 'col-xs-1' },
    { columnName: 'serialNumber', order: '7', displayName: 'Serial Number', cssClassName: 'col-xs-2' },
    { columnName: 'deviceLocation', order: '8', displayName: 'Device Location', cssClassName: 'col-xs-2' },
    { columnName: 'active', order: '9', displayName: 'Active', cssClassName: 'col-xs-1' },
    { columnName: 'nextTestDue',
order: '4',
customComponent: nextTestDueComponent,
displayName: 'Test Due',
    cssClassName: 'col-xs-3' },
    { columnName: 'actions',
customComponent: actionButtons,
sortable: false,
cssClassName: 'col-xs-3',
    displayName: 'Actions' }
];

const DevicesDueForTestList = ({ devices }) => (
devices.length > 0 ?
    <Griddle
        results={devices}
        useGriddleStyles={false}
        tableClassName={'griddle-flex table table-bordered table-striped table-hover highlight striped bordered'}
        settingsToggleClassName='btn btn-default'
        useCustomPagerComponent={false}
        showFilter
        showSettings
        resultsPerPage={50}
        columnMetadata={columnMetadata}
        columns={columnList}
    /> :
   <Alert bsStyle="warning">No devices due for tests yet.</Alert>
);

DevicesDueForTestList.propTypes = {
    devices: PropTypes.array,
};

export default container((props, onData) => {
    const subscription = Meteor.subscribe('devicesduefortests.list');
    if (subscription.ready()) {
        const devices = Devices.find().fetch();
        _.map(devices, (device) => {
            if (device.facilityId) {
                const facility = Facilities.findOne(device.facilityId);
                const customer = Customers.findOne(facility.customerID);
                device.facilityName = facility ? facility.facilityName : '';
                device.customerName = customer ? customer.customerName : '';
            }
        });
        onData(null, { devices });
    }
}, DevicesDueForTestList);
