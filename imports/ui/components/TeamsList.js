import React from 'react';
import PropTypes from 'prop-types';
import { Alert } from 'react-bootstrap';
import { Meteor } from 'meteor/meteor';
import { browserHistory } from 'react-router';
import { GridList, GridTile } from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';
import Teams from '../../api/teams/teams';
import { Team } from './Team';
import container from '../../modules/container';

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    width: '100%',
    height: '100%',
    overflowY: 'auto',
  },
};


const TeamsList = ({ teams }) => (
  teams.length === 0 ? <Alert bsStyle="warning">No teams yet.</Alert> :
  <div style={styles.root}>
    <GridList
      cellHeight={180}
      style={styles.gridList}
    >
      <Subheader>My Teams</Subheader>
      {teams.map((team) => (
          <GridTile
            onTouchTap={(e) => {
              browserHistory.push(`teams/${team._id}`);
            }}
            style={{ cursor: 'pointer' }}
            key={team.img}
            title={team.name}
            cols='0.4'
            subtitle={<span>members <b>{team.contacts && team.contacts.length}</b></span>}
            actionIcon={<IconButton><StarBorder color="white" /></IconButton>}
          >
            <img src={'/images/default_team_image.jpg'} />
          </GridTile>
      ))}
    </GridList>
  </div>
);

TeamsList.propTypes = {
  teams: PropTypes.array
};

export default container((props, onData) => {
  const subscription = Meteor.subscribe('teams.list');
  if (subscription.ready()) {
    const teams = Teams.find().fetch();
    onData(null, { teams });
  }
}, TeamsList);
