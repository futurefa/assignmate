import React from 'react';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';
import _ from 'lodash';

import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import DatePicker from 'material-ui/DatePicker';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Checkbox from 'material-ui/Checkbox';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';
import Avatar from 'material-ui/Avatar';
import { List, ListItem } from 'material-ui/List';
import ActionAssignment from 'material-ui/svg-icons/action/assignment';
import ActionInfo from 'material-ui/svg-icons/action/info';
import { blue500, yellow600 } from 'material-ui/styles/colors';
import Griddle from 'griddle-react';

import Tasks from '../../api/tasks/tasks';
import Uploads from './Uploads';
import UploadsList from './UploadsList';
import TeamAutocomplete from './TeamAutocomplete';
import TeamEditor from '../components/TeamEditor';

export default class TaskEditor extends React.Component {
  constructor(props) {
    super(props);
    this.handleInput = _.debounce(this.handleInput.bind(this), 2500);
    this.state = {
      taskId: this.props.task ? this.props.task._id : '',
      team: (this.props.task && this.props.task.team) || null,
      dueDate: this.props.task ? this.props.task.dueDate : null,
      attachments: this.props.task ? (this.props.task.attachments ? this.props.task.attachments : []) : [],
      currentAttachment: undefined,
      };
    this.setAttachment = this.setAttachment.bind(this);
  }

  getValues() {
    let values = {},
     dateValues = ['dueDate'],
     dropDownValues = [];
    for (const key in this.refs) {
      values[key] = this.refs[key].getValue();
    }
    values.attachments = this.state.attachments;
    values.team = this.state.team ? { _id: this.state.team._id, name: this.state.team.name } : null;
    values.owner = { _id: Meteor.userId(), name: Meteor.user().profile.name.first };
    _.each(dateValues, (dateValue) => { values[dateValue] = this.state[dateValue]; });
    _.each(dropDownValues, (dropDownValue) => { values[dropDownValue] = this.state[dropDownValue]; });
    return values;
  }

  setAttachment(attachment) {
    this.setState({ currentAttachment: attachment });
  }

  updateAttachments() {
    const attachments = this.state.attachments || [];
    const attachment = this.state.currentAttachment;
    if (!attachment) { return false; }
    if (attachment._id) {
      const index = _.findIndex(attachments, _.pick(attachment, '_id'));
      attachments.splice(index, 1, attachment);
    }
    this.setState({ attachments }, () => {
      this.save();
    });
  }

  handleDateInput(name, e, date) {
    v = {};
    v[name] = date;
    this.setState(v, () => {
      this.save();
    });
  }
  handleSelectInput(name, e, key, payload) {
    v = {};
    v[name] = payload;
    this.setState(v, () => { this.save(); });
  }

  handleCheckBoxInput(name, e, value) {
    v = {};
    v[name] = value;
    this.setState(v, () => { this.save(); });
  }

  handleInput(e) {
    e.preventDefault;
    this.save();
  }

  setSelectedTeam(team, idx) {
    console.log(this);
    this.setState({ team }, () => {
      this.save();
    });
  }

  save() {
    const task = this.getValues();
    const { taskId } = this.state;
    Meteor.call('saveTask', taskId, task, (err, res) => {
      if (!err) {
        Bert.alert({
          title: 'All Changes Saved',
          type: 'success',
          style: 'growl-top-right',
          icon: 'fa-check'
        });
        if (!taskId) { this.setState({ taskId: res.insertedId }); }
      } else {

      }
    });
  }

  handleOpenAttachmentModal() {
    this.setState({ currentAttachment: {} });
    this.setState({ open: true });
  }

  handleCloseAttachmentModal() {
    this.setState({ open: false });
    this.updateAttachments();
  }

  handleOpenTeamModal() {
    this.setState({ openTeam: true });
  }

  handleCloseTeamModal() {
    this.setState({ openTeam: false });
  }

  editAttachment(attachment) {
    this.setState({ currentAttachment: attachment });
    this.setState({ open: true });
  }

  removeAttachment(attachmentId) {
    const attachments = _.remove(this.state.attachments, (attachment) => attachment._id != attachmentId);
    this.setState({ attachments }, () => {
      this.save();
    });
  }

  render() {
    const self = this;
    const { task } = this.props;
    const columnList = [
      'title', 'body', 'dueDate', 'active', 'actions'
    ];
    const actionButtons = React.createClass({
      render() {
        return (
          <div>
            <RaisedButton
              label='Edit'
              style={{ marginRight: 10 }}
              onClick={() => self.editAttachment(this.props.rowData)}
            />
            <RaisedButton
              label='Delete'
              primary
              onClick={() => self.removeAttachment(this.props.rowData.contactId)}
            />
          </div>
        );
      }
    });

    const attachmentModalActions = [
      <FlatButton
        label='Cancel'
        primary
        onTouchTap={this.handleCloseAttachmentModal.bind(this)}
      />,
      <FlatButton
        label='Save'
        primary
        keyboardFocused
        onTouchTap={this.handleCloseAttachmentModal.bind(this)}
      />
    ];
    const teamModalActions = [
      <FlatButton
        label='Cancel'
        primary
        onTouchTap={this.handleCloseTeamModal.bind(this)}
      />,
      <FlatButton
        label='Save'
        primary
        keyboardFocused
        onTouchTap={this.handleCloseTeamModal.bind(this)}
      />
    ];

    const columnMetadata = [
            { columnName: 'title', order: '1', displayName: 'Task Title', cssClassName: 'col-xs-2' },
            { columnName: 'body', order: '2', displayName: 'Task Description', cssClassName: 'col-xs-2' },
            { columnName: 'dueDate', order: '3', displayName: 'Due', cssClassName: 'col-xs-2' },
            { columnName: 'active', order: '4', displayName: 'Active', cssClassName: 'col-xs-2' },
            { columnName: 'actions', customComponent: actionButtons, displayName: 'Actions', sortable: false, cssClassName: 'col-xs-2' }
    ];

    const style = {
            height: 'auto',
            width: '100%',
            padding: 20,
            display: 'inline-block',
            marginTop: 10,
        };

    return (
      <div>
      <Row>
        <Paper style={style} zDepth={1}>
          <Row>
            <Col xs={12}>
              <h4 className="pull-left">Linked Team</h4>
            </Col>
            <Col xs={12}>
              <p>Every Task needs to be linked to a team</p>
            </Col>
          </Row>
          <Row>
            <Col xs={12} sm={12} md={6}>
              <TeamAutocomplete
                teamId={task && task.team && task.team._id}
                setTeam={team => {
                  this.setSelectedTeam(team);
                }}
              />
            </Col>
            <Col xs={1} sm={1} md={1}>
              <p>-OR-</p>
            </Col>
            <Col xs={12} sm={12} md={5}>
              <RaisedButton label="Create New Team" primary onTouchTap={this.handleOpenTeamModal.bind(this)} />
            </Col>
          </Row>
        </Paper>
        <Col xs={12} >
          <TextField
            ref='title'
            floatingLabelText='Task Title'
            onChange={this.handleInput}
            defaultValue={task && task.title}
            fullWidth
            hintText='Task Title'
          />
        </Col>
      </Row>
      <Row>
        <Col xs={12} >
          <TextField
            ref='body'
            name='body'
            floatingLabelText='Summary'
            multiLine
            rows='8'
            onChange={this.handleInput}
            defaultValue={task && task.body}
            fullWidth
            hintText='Summary'
          />
        </Col>
      </Row> 
      <Row>
        <Col xs={6} >
          <DatePicker
            floatingLabelText="Due Date"
            onChange={this.handleDateInput.bind(this, 'dueDate')}
            value={this.state.dueDate ? new Date(this.state.dueDate) : null}
            fullWidth
            hintText="Due Date"
          />
        </Col>
      </Row>
      <Row>
        <Col xs={12} md={12} sm={12}>
          <Col xs={12} sm={12} md={12}>
            <RaisedButton label='Add Attachment' primary onTouchTap={this.handleOpenAttachmentModal.bind(this)} />
          </Col>
          <Col xs={12} sm={12} md={12}>
            {this.state.attachments.length
                            ? <Griddle
                              results={this.state.attachments}
                              useGriddleStyles={false}
                              tableClassName={'griddle-flex table table-bordered table-striped table-hover highlight striped bordered'}
                              settingsToggleClassName='btn btn-default'
                              useCustomPagerComponent={false}
                              showFilter
                              showSettings
                              resultsPerPage={50}
                              columnMetadata={columnMetadata}
                              columns={columnList}
                            />
                                : <div />
                            }
            <UploadsList item={task} />
          </Col>
        </Col>
        <Dialog
          title='Upload File'
          actions={attachmentModalActions}
          modal={false}
          open={this.state.open || false}
          onRequestClose={this.handleCloseAttachmentModal}
          autoScrollBodyContent
        >
          <Row>
            <Col xs={12}>
              <Uploads onChange={this.setAttachment} item={task} />
            </Col>
          </Row>
        </Dialog>
        <Dialog
          title="Create New Team"
          actions={teamModalActions}
          modal={false}
          open={this.state.openTeam}
          onRequestClose={this.handleCloseTeamModal}
          autoScrollBodyContent
        >
              <Row>
                  <Col xs={12}>
                      <TeamEditor />
                  </Col>
              </Row>
          </Dialog>
      </Row>
      </div>
    );
  }
}

TaskEditor.propTypes = {
  task: PropTypes.object,
  getTeam: PropTypes.func
};
