import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import Teams from '../../api/teams/teams';
import container from '../../modules/container';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import AutoComplete from 'material-ui/AutoComplete';

class TeamAutocomplete extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedValue: props.team ? props.team.name : ''
        };
        this.handleInput = this.handleInput.bind(this);
    }

    handleInput(searchText) {
        this.setState({ selectedValue: searchText });
    }

    render() {
        const { setTeam } = this.props;
        const { teams } = this.props;
        const { selectedValue } = this.state;
        return (
            <AutoComplete
                floatingLabelText="Select Team"
                filter={AutoComplete.fuzzyFilter}
                searchText={selectedValue}
                maxSearchResults={8}
                openOnFocus
                onUpdateInput={this.handleInput}
                onNewRequest={setTeam}
                dataSource={teams}
                dataSourceConfig={{
                    text: 'name',
                    value: '_id',
                }}
                fullWidth
            />
        );
    }
}

TeamAutocomplete.propTypes = {
    teams: PropTypes.array,
    setTeam: PropTypes.func,
    teamId: PropTypes.string
};

export default container((props, onData) => {
    const subscription = Meteor.subscribe('teams.list');
    if (subscription.ready()) {
        const teams = Teams.find().fetch();
        const team = Teams.findOne(props.teamId);
        onData(null, { teams, team });
    }
}, TeamAutocomplete);
