import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Button, ButtonGroup, Alert } from 'react-bootstrap';
import { Meteor } from 'meteor/meteor';
import Testers from '../../api/testers/testers';
import { removeTester } from '../../api/testers/methods';
import container from '../../modules/container';
import Griddle from 'griddle-react';

const moment = require('moment');

const handleNav = (_id, datas) => console.log(_id);

const handleRemove = (_id) => {
  if (confirm('Are you sure? This is permanent!')) {
    Meteor.call('removeTester', _id, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert('Tester deleted!', 'success');
        browserHistory.push('/testers');
      }
    });
  }
};

const certExpComponent = React.createClass({
  render() {
    const date = moment(this.props.rowData.certExp).format('YYYY-MM-DD');
    return (
        <div>
            {date}
        </div>
    );
  }
});

const actionButtons = React.createClass({
  render() {
    return (
        <div>
          <ButtonGroup>
            <Button bsSize="xsmall" href={`/testers/${this.props.rowData._id}`}>Edit</Button>
            <Button bsSize="xsmall" onClick={() => handleRemove(this.props.rowData._id)} bsStyle="danger">Delete</Button>
          </ButtonGroup>
        </div>
    );
  }
});

const columnList = ['name', 'certNumber', 'certExp', 'active', 'actions'];
const columnMetadata = [
  { columnName: 'name', order: '1', displayName: 'Tester Name', cssClassName: 'col-xs-2' },
  { columnName: 'certNumber', order: '2', displayName: 'Cert Number', cssClassName: 'col-xs-1' },
  { columnName: 'certExp', order: '4', customComponent: certExpComponent, displayName: 'Cert Expiration', cssClassName: 'col-xs-1' },
  { columnName: 'active', order: '3', displayName: 'Active', cssClassName: 'col-xs-1' },
  { columnName: 'actions',
customComponent: actionButtons,
sortable: false,
cssClassName: 'col-xs-3',
    displayName: 'Actions' }
];

 const TestersList = ({ testers }) => (
    testers.length > 0 ?
        <Griddle
            results={testers}
            useGriddleStyles={false}
            tableClassName={'griddle-flex table table-bordered table-striped table-hover highlight striped bordered'}
            settingsToggleClassName='btn btn-default'
            useCustomPagerComponent={false}
            showFilter
            showSettings
            resultsPerPage={50}
            columnMetadata={columnMetadata}
            columns={columnList}
        /> :
   <Alert bsStyle="warning">No testers yet.</Alert>
 );


TestersList.propTypes = {
  testers: PropTypes.array,
};

export default container((props, onData) => {
  const subscription = Meteor.subscribe('testers.list');
  if (subscription.ready()) {
    const testers = Testers.find().fetch();
    onData(null, { testers });
  }
}, TestersList);
