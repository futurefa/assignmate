import React from 'react';
import Files from '../../api/files/files';
import { Meteor } from 'meteor/meteor';
import Dropzone from 'react-dropzone';
import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';
import Avatar from 'material-ui/Avatar';
import { List, ListItem } from 'material-ui/List';
import ActionAssignment from 'material-ui/svg-icons/action/assignment';
import ActionInfo from 'material-ui/svg-icons/action/info';
import { blue500, yellow600 } from 'material-ui/styles/colors';
const Uploads = React.createClass({
    getInitialState() {
        return {
          files: [],
          item: this.props.item
        };
    },

    _handleUpload(acceptedFiles, rejectedFiles) { //this function is called whenever a file was dropped in your dropzone
        const comp = this;
        comp.setState({
          files: comp.state.files.concat(acceptedFiles)
        });
        const currentFiles = this.state.files;
        acceptedFiles.map((f) => currentFiles.push(f));
        this.setState({
          files: currentFiles,
        });
        _.each(acceptedFiles, (file) => {
          const newFile = new FS.File(file);
          newFile.owner = Meteor.userId();
          newFile.itemId = comp.state.item && comp.state.item._id;
           
            Files.insert(newFile, (err, fileObj) => {
                if (err) {
                    console.log(err); //in case there is an error, log it to the console
                } else {
                    console.log('saved ', fileObj);
                }
            });
        });
    },

    render() {
        console.log(this.props);
        return (
            <div>
              <Row>
                <Col xs={12} >
                  <Dropzone onDrop={this._handleUpload}>
                        <div>Try dropping some files here, or click to select files to upload.</div>
                  </Dropzone>
                </Col>
              </Row>
              <Row>
                <Col xs={12} >
                    <Divider inset />
                    <List>
                      <Subheader inset>Files</Subheader>
                        {this.state.files.map((file) => {
                            <ListItem
                              leftAvatar={<Avatar icon={<ActionAssignment />} backgroundColor={blue500} />}
                              rightIcon={<ActionInfo />}
                              primaryText={file.name}
                              secondaryText="Jan 20, 2014"
                            />;
                        })}
                    </List>
                </Col>
              </Row>
          </div>
        );
    }
});
export default Uploads;
