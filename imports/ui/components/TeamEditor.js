import React from 'react';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';
import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Checkbox from 'material-ui/Checkbox';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import Teams from '../../api/teams/teams';
import _ from 'lodash';
import { Random } from 'meteor/random';
import Griddle from 'griddle-react';
import ContactEditor from './ContactEditor';
import InviteMember from '../pages/InviteMember.js';

export default class TeamEditor extends React.Component {
  constructor(props) {
    super(props);
    this.handleInput = _.debounce(this.handleInput.bind(this), 2500);
    this.state = {
      teamId: props.team ? props.team._id : '',
      contacts: (props && props.members) || [],
      currentContact: undefined,
      };
    this.setContact = this.setContact.bind(this);
  }

  getValues() {
    let values = {},
     booleanValues = ['active'],
     dateValues = [],
     dropDownValues = [];
    for (const key in this.refs) {
      values[key] = this.refs[key].getValue();
    }
    values.contacts = this.state.contacts;
    _.each(booleanValues, (booleanValue) => { values[booleanValue] = this.state[booleanValue]; });
    _.each(dateValues, (dateValue) => { values[dateValue] = this.state[dateValue]; });
    _.each(dropDownValues, (dropDownValue) => { values[dropDownValue] = this.state[dropDownValue]; });
    return values;
  }

  setContact(contact) {
    if (contact) {
      const self = this;
      this.setState({ currentContact: contact }, () => {
        self.updateContacts();
      });
    }
  }

  updateContacts() {
    const contacts = this.state.contacts || [];
    let contact = this.state.currentContact;
    const self = this;
    function save(comp, items) {
      return comp.setState({ contacts: items }, () => {
        comp.saveTeam();
      });
    }
    if (!contact) { return false; }
    if (contact._id) {
      const index = _.findIndex(contacts, _.pick(contact, '_id'));
      contacts.splice(index, 1, contact);
      save(self, contacts);
    } else {
      Meteor.call('inviteMember', contact.email, ['member'], (err, user) => {
        if (user) {
          Bert.alert({
            title: 'Invite sent',
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-check'
          });
          contact = {};
          contact._id = user._id;
          contacts.push(contact);
          save(self, contacts);
        }else{
          Bert.alert({
            title: err.message,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-check'
          });
        }
      });
    }
  }

  handleDateInput(name, e, date) {
    const v = {};
    v[name] = date;
    this.setState(v, () => {
      this.saveTeam();
    });
  }
  handleSelectInput(name, e, key, payload) {
    const v = {};
    v[name] = payload;
    this.setState(v, () => { this.saveTeam(); });
  }

  handleCheckBoxInput(name, e, value) {
    v = {};
    v[name] = value;
    this.setState(v, () => { this.saveTeam(); });
  }

  handleInput(e) {
    e.preventDefault;
    this.saveTeam();
  }

  saveTeam() {
    const team = this.getValues();
    const { teamId } = this.state;
    Meteor.call('saveTeam', teamId, team, (err, res) => {
      if (!err) {
        Bert.alert({
          title: 'All Changes Saved',
          type: 'success',
          style: 'growl-top-right',
          icon: 'fa-check'
        });
        if (!teamId) { this.setState({ teamId: res.insertedId }); }
      } else {

      }
    });
  }

  handleOpen() {
    this.setState({ currentContact: {} });
    this.setState({ open: true });
  }

  handleCloseContactForm() {
    this.setState({ open: false });
  }

  handleSaveContactForm() {
    this.setState({ open: false });
    this.updateContacts();
  }

  componentDidUpdate() {
        // this.saveTeam();
  }

  editContact(contact) {
    this.setState({ currentContact: contact });
    this.setState({ open: true });
  }

  removeContact(contactId) {
    const contacts = _.remove(this.state.contacts, contact => contact._id !== contactId);
    this.setState({ contacts }, () => {
      this.saveTeam();
    });
  }

  render() {
    const self = this;
    const { team, members } = this.props;
    const columnList = [
      'profile.name.first', 'profile.email', 'phone', 'actions'
    ];
    const actionButtons = React.createClass({
      render() {
        return (
          <div>
            <RaisedButton
              label='Edit'
              style={{ marginRight: 10 }}
              onClick={() => self.editContact(this.props.rowData)}
            />
            <RaisedButton
              label='Delete'
              primary
              onClick={() => self.removeContact(this.props.rowData._id)}
            />
          </div>
        );
      }
    });

    const dialogActions = [
      <FlatButton
        label='Close'
        primary
        onTouchTap={this.handleCloseContactForm.bind(this)}
      />
    ];

    const columnMetadata = [
            { columnName: 'profile.name.first', order: '1', displayName: 'Contact Name', cssClassName: 'col-xs-2' },
            { columnName: 'profile.email', order: '2', displayName: 'Email', cssClassName: 'col-xs-2' },
            { columnName: 'phone', order: '3', displayName: 'Phone', cssClassName: 'col-xs-2' },
            { columnName: 'actions', customComponent: actionButtons, displayName: 'Actions', sortable: false, cssClassName: 'col-xs-2' }
    ];

    return (
      <Row>
        <Col xs={12} md={3} sm={6}>
          <TextField
            ref='name'
            floatingLabelText='Team name'
            onChange={this.handleInput}
            defaultValue={team && team.name}
            fullWidth
            hintText='Team Name'
          />
        </Col>
     
        <Col xs={12} md={3} sm={6}>
          <TextField
            ref='phone'
            name='phone'
            floatingLabelText='Phone'
            onChange={this.handleInput}
            defaultValue={team && team.phone}
            fullWidth
            hintText='Phone'
          />
        </Col>
       
        <Col xs={12} md={3} sm={6}>
          <TextField
            ref='email'
            name='email'
            floatingLabelText='email'
            onChange={this.handleInput}
            defaultValue={team && team.email}
            fullWidth
            hintText='email'
          />
        </Col>
        
        <Col xs={12} md={12} sm={12}>
          <TextField
            ref='description'
            multiLine
            rows={2}
            floatingLabelText='Description'
            onChange={this.handleInput}
            defaultValue={team && team.description}
            fullWidth
            hintText='description'
          />
        </Col>
        <Col xs={12} md={12} sm={12}>
          <Checkbox
            label='Active'
            onCheck={this.handleCheckBoxInput.bind(this, 'active')}
            defaultChecked={team && team.active}
          />
        </Col>
        <Col xs={12} md={12} sm={12}>
          <Col xs={12} sm={12} md={12}>
            <RaisedButton label='Invite Members' disabled={!this.state.teamId} primary onTouchTap={this.handleOpen.bind(this)} />
          </Col>
          <Col xs={12} sm={12} md={12}>
            {this.state.contacts.length
                            ? <Griddle
                              results={this.state.contacts}
                              useGriddleStyles={false}
                              tableClassName={'griddle-flex table table-bordered table-striped table-hover highlight striped bordered'}
                              settingsToggleClassName='btn btn-default'
                              useCustomPagerComponent={false}
                              showFilter
                              showSettings
                              resultsPerPage={50}
                              columnMetadata={columnMetadata}
                              columns={columnList}
                            />
                                : <div />
                            }
          </Col>
        </Col>
        <Dialog
          title={`Invite Member to ${team && team.name}`}
          modal={false}
          actions={dialogActions}
          open={this.state.open && team && team._id}
          onRequestClose={() => self.setState({ open: false })}
          autoScrollBodyContent
        >
          <Row>
            <Col xs={12}>
              <InviteMember onChange={this.setContact} />
{/*              <ContactEditor contact={this.state.currentContact} setContact={this.setContact} />
*/}            </Col>
          </Row>
        </Dialog>
      </Row>
    );
  }
}

TeamEditor.propTypes = {
  team: PropTypes.object,
  getCustomer: PropTypes.func
};
