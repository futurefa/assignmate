import React from 'react';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';
import { Grid, Row, Col } from 'react-bootstrap';
import { TextField, DatePicker, Checkbox, RaisedButton } from 'material-ui';
import { browserHistory, Link } from 'react-router';

export default class CompanyEditor extends React.Component {
    constructor(props) {
        super(props);
        this.handleInput = this.handleInput.bind(this);
        this.state = {
            companyId: this.props.company ? this.props.company._id : '',
            licenceExpiry: null
        };
    }

    getValues() {
        const values = {};
        const booleanValues = ['active'];
        const dateValues = ['licenceExpiry', 'insuranceInformation.expiryDate'];
        for (const key in this.refs) {
            values[key] = this.refs[key].getValue();
        }
        _.each(booleanValues, (booleanValue) => { values[booleanValue] = this.state[booleanValue]; });
        _.each(dateValues, (dateValue) => { values[dateValue] = new Date(this.state[dateValue]); });
        return values;
    }

    handleDateInput(name, e, date) {
        v = {};
        v[name] = date;
        this.setState(v, () => { this.saveCompany(); });
    }

    handleCheckBoxInput(name, e, value) {
        v = {};
        v[name] = value;
        this.setState(v, () => { this.saveCompany(); });
    }

    handleInput(e) {
        e.preventDefault;
        this.saveCompany();
    }

    saveCompany() {
        const company = this.getValues();
        //company['customerID'] = this.props.getCustomer() ? this.props.getCustomer()._id : null;
        const { companyId } = this.state;
        Meteor.call('saveCompany', companyId, company, (err, res) => {
            if (!err) {
                _.debounce(
                    Bert.alert({
                        title: 'All Changes Saved',
                        type: 'success',
                        style: 'growl-top-right',
                        icon: 'fa-check'
                    }), 1000);
                if (!companyId) { this.setState({ companyId: res.insertedId }); }
            return;
            }
            
                console.log('company', company);
                console.log('error', err);
        });
    }

    componentWillUnmount() {
        _.debounce(
            Bert.alert({
                title: 'All Changes Saved',
                type: 'success',
                style: 'growl-top-right',
                icon: 'fa-check'
            }), 1000);
    }


    render() {
        const { company } = this.props;
        const panelStyle = {
            border: '1px solid #ccc',
            borderRadius: '5px',
            padding: '10px'
        };
        return (
            <Grid>
            {this.state.companyId ?
                <Row>
                    <Col>
                        <RaisedButton
                          onTouchTap={() => (this.setState({ companyId: '' }))}
                          className="pull-right"
                          primary
                          label="New"
                        />
                    </Col>
                </Row> : 
                null
            }
                
                <Row>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="companyName"
                            floatingLabelText="Company name"
                            onChange={this.handleInput}
                            defaultValue={company && company.companyName}
                            fullWidth
                            hintText="Company Name"
                        />
                    </Col>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="contactPerson"
                            floatingLabelText="Contact Person"
                            onChange={this.handleInput}
                            defaultValue={company && company.contactPerson}
                            fullWidth
                            hintText="Contact Person"
                        />
                    </Col>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="phone"
                            floatingLabelText="Phone Number"
                            onChange={this.handleInput}
                            defaultValue={company && company.phone}
                            fullWidth
                            hintText="Phone Number"
                        />
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="otherPhone"
                            floatingLabelText="Other Phone"
                            onChange={this.handleInput}
                            defaultValue={company && company.otherPhone}
                            fullWidth
                            hintText="Other Phone"
                        />
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="businessLicenceNo"
                            floatingLabelText="Business License No"
                            onChange={this.handleInput}
                            defaultValue={company && company.businessLicenceNo}
                            fullWidth
                            hintText="Business License No"
                        />
                    </Col>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="email"
                            floatingLabelText="Email"
                            onChange={this.handleInput}
                            defaultValue={company && company.email}
                            fullWidth
                            hintText="Email"
                        />
                    </Col>
                    <Col xs={12} md={3} sm={6}>
                        <DatePicker
                            name="licenceExpiry"
                            floatingLabelText="Licence Expiry"
                            onChange={this.handleDateInput.bind(this, 'licenceExpiry')}
                            value={this.state.licenceExpiry}
                            defaultValue={company && company.licenceExpiry}
                            fullWidth
                            hintText="Licence Expiry"
                        />
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="fax"
                            floatingLabelText="Fax"
                            onChange={this.handleInput}
                            defaultValue={company && company.fax}
                            fullWidth
                            hintText="Fax"
                        />
                    </Col>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="address1"
                            floatingLabelText="Address One"
                            onChange={this.handleInput}
                            defaultValue={company && company.address1}
                            fullWidth
                            hintText="Address One"
                        />
                    </Col>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="address2"
                            floatingLabelText="Address two"
                            onChange={this.handleInput}
                            defaultValue={company && company.address2}
                            fullWidth
                            hintText="Address two"
                        />
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="city"
                            floatingLabelText="City"
                            maxLength="5"
                            onChange={this.handleInput}
                            defaultValue={company && company.city}
                            fullWidth
                            hintText="City"
                        />
                    </Col>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="state"
                            maxLength="2"
                            floatingLabelText="State"
                            onChange={this.handleInput}
                            defaultValue={company && company.state}
                            fullWidth
                            hintText="State"
                        />
                    </Col>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="zipCode"
                            floatingLabelText="Zip code"
                            onChange={this.handleInput}
                            defaultValue={company && company.zipCode}
                            fullWidth
                            hintText="Zip code"
                        />
                    </Col>
                </Row>
                <Row>
                <Col style={panelStyle}>
                    <h3>Insurance Information</h3>
                    <Row>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="insuranceInformation.insuranceCompany"
                            floatingLabelText="Insurance Company"
                            onChange={this.handleInput}
                            defaultValue={company && company.insuranceInformation && company.insuranceInformation.insuranceCompany}
                            fullWidth
                            hintText="Insurance Company"
                        />
                    </Col>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="insuranceInformation.contactName"
                            floatingLabelText="Contact name"
                            onChange={this.handleInput}
                            defaultValue={company && company.insuranceInformation && company.insuranceInformation.contactName}
                            fullWidth
                            hintText="Contact name"
                        />
                    </Col>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="insuranceInformation.phone"
                            floatingLabelText="Phone"
                            onChange={this.handleInput}
                            defaultValue={company && company.insuranceInformation && company.insuranceInformation.phone}
                            fullWidth
                            hintText="Phone"
                        />
                    </Col>
                    </Row>
                    <Row>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="insuranceInformation.fax"
                            floatingLabelText="Fax"
                            onChange={this.handleInput}
                            defaultValue={company && company.insuranceInformation && company.insuranceInformation.fax}
                            fullWidth
                            hintText="Fax"
                        />
                    </Col>
                    
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="insuranceInformation.policyNo"
                            floatingLabelText="Policy No"
                            onChange={this.handleInput}
                            defaultValue={company && company.insuranceInformation && company.insuranceInformation.policyNo}
                            fullWidth
                            hintText="policy Number"
                        />
                    </Col>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="insuranceInformation.liabilityAmount"
                            floatingLabelText="Liability Amount"
                            onChange={this.handleInput}
                            defaultValue={company && company.insuranceInformation && company.insuranceInformation.liabilityAmount}
                            fullWidth
                            hintText="Liability Amount"
                        />
                    </Col>
                    </Row>
                    <Row>
                    <Col xs={12} md={3} sm={6}>
                        <DatePicker
                            name="insuranceInformation.expiryDate"
                            floatingLabelText="Expiry date"
                            onChange={this.handleDateInput.bind(this, 'insuranceInformation.expiryDate')}
                            value={this.state['insuranceInformation.expiryDate']}
                            defaultValue={company && company.insuranceInformation && company.insuranceInformation.expiryDate}
                            fullWidth
                            hintText="Expiry date"
                        />
                    </Col>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="insuranceInformation.comments"
                            multiLine
                            rows={2}
                            floatingLabelText="Comment"
                            onChange={this.handleInput}
                            fullWidth
                            defaultValue={company && company.insuranceInformation && company.insuranceInformation.comments}
                            hintText="comments"
                        />
                    </Col>
                    </Row>
                </Col>
                </Row>
                <Row>
                        <Col xs={12} md={4} sm={6}>
                        <TextField
                            ref="comments"
                            multiLine
                            rows={2}
                            floatingLabelText="Comment"
                            onChange={this.handleInput}
                            fullWidth
                            defaultValue={company && company.comments}
                            hintText="comments"
                        />
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} md={12} sm={12}>
                        <Checkbox
                            label="Active"
                            onCheck={this.handleCheckBoxInput.bind(this, 'active')}
                            defaultChecked={company && company.active}
                            hintText="Active"
                        />
                    </Col>
                </Row>
            </Grid>
        );
    }
}

CompanyEditor.propTypes = {
    company: PropTypes.object,
    getCustomer: PropTypes.func
};
