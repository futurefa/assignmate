import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Meteor } from 'meteor/meteor';
import Avatar from 'material-ui/Avatar';
import { List, ListItem } from 'material-ui/List';
import AccountCircle from 'material-ui/svg-icons/action/account-circle';
import ActionInfo from 'material-ui/svg-icons/action/info';
import ActionCancel from 'material-ui/svg-icons/navigation/cancel';
import { blue500, yellow600 } from 'material-ui/styles/colors';
import { Alert } from 'react-bootstrap';

import container from '../../modules/container';

const getName = user => {
  const first = (user.profile && user.profile.name && user.profile.name.first) || '';
  const last = (user.profile && user.profile.name && user.profile.name.last) || '';
  return `${first} ${last}`;
};
const ContactList = ({ users }) => (
  users.length > 0 ? <List className="DocumentsList">
    {users.map((user) => (
      <ListItem
              key={user._id}
              leftAvatar={<Avatar icon={<AccountCircle color='#ffffff' />} backgroundColor={yellow600} />}
              rightIcon={<ActionCancel
onClick={(event) => {
                event.stopPropagation();
              }}
              />}
              primaryText={getName(user)}
      />
    ))}
  </List> :
  <Alert bsStyle="warning">No users yet.</Alert>
);

ContactList.propTypes = {
    users: PropTypes.array,
};

export default container((props, onData) => {
  console.log(props);
    const subscription = Meteor.subscribe('teams.view.members', props.item && props.item._id);
    if (subscription.ready()) {
      const ids = (props.item && props.item.contacts && _.pluck(props.item.contacts, '_id')) || [];
        const selector = { _id: { $in: ids } } || {};
        const users = Meteor.users.find(selector).fetch();
        console.log(users, 'users');
        onData(null, { users });
    }
}, ContactList);
