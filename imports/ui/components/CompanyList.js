import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Button, ButtonGroup, Alert } from 'react-bootstrap';
import { Meteor } from 'meteor/meteor';
import Company from '../../api/company/company';
import { removeCompany } from '../../api/company/methods';
import container from '../../modules/container';
import Griddle from 'griddle-react';
import RaisedButton from 'material-ui/RaisedButton';

const handleNav = (_id, datas) => console.log(_id);

const handleRemove = (_id) => {
  if (confirm('Are you sure? This is permanent!')) {
    removeCompany.call({ _id }, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert('Company deleted!', 'success');
        browserHistory.push('/companies');
      }
    });
  }
};

const style = {
  marginRight: 10
};

const actionButtons = React.createClass({
  render() {
    return (
      <div>
        <RaisedButton
          label='Edit'
          style={style}
          onClick={() => browserHistory.push(`/companies/${this.props.rowData._id}`)}
        />
        <RaisedButton
          label='Delete'
          primary
          onClick={() => handleRemove(this.props.rowData._id)}
        />
      </div>
    );
  }
});

const columnList = ['companyName', 'address1', 'city', 'state', 'zipCode', 'active', 'actions'];
const columnMetadata = [
  { columnName: 'companyName', order: '1', displayName: 'Company Name', cssClassName: 'col-xs-2' },
  { columnName: 'address1', order: '2', displayName: 'Primary Address', cssClassName: 'col-xs-2' },
  { columnName: 'state', order: '3', displayName: 'State', cssClassName: 'col-xs-1' },
  { columnName: 'city', order: '4', displayName: 'City', cssClassName: 'col-xs-1' },
  { columnName: 'zipCode', order: '5', displayName: 'Zip Code', cssClassName: 'col-xs-1' },
  { columnName: 'active', order: '6', displayName: 'Active', cssClassName: 'col-xs-1' },
  { columnName: 'actions',
    customComponent: actionButtons,
    sortable: false,
    cssClassName: 'col-xs-3',
    displayName: 'Actions' }
];

const CompanyList = ({ companies }) => (
companies.length > 0
    ? <Griddle
      results={companies}
      useGriddleStyles={false}
      tableClassName={'griddle-flex table table-bordered table-striped table-hover highlight striped bordered'}
      settingsToggleClassName='btn btn-default'
      useCustomPagerComponent={false}
      showFilter
      showSettings
      resultsPerPage={50}
      columnMetadata={columnMetadata}
      columns={columnList}
    />
   : <Alert bsStyle='warning'>No companies yet.</Alert>
);

CompanyList.propTypes = {
  companies: PropTypes.array
};

export default container((props, onData) => {
  const subscription = Meteor.subscribe('company.viewAll');
  if (subscription.ready()) {
    const companies = Company.find().fetch();
    onData(null, { companies });
  }
}, CompanyList);
