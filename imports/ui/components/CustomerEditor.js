import React from 'react';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';
import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import Customers from '../../api/customers/customers';
import _ from 'lodash';


export default class CustomerEditor extends React.Component {
    constructor(props) {
        super(props);
        this.handleInput = _.debounce(this.handleInput.bind(this), 2500);
        this.state = {
            customerId: this.props.customer ? this.props.customer._id : ''
        };
    }

    getValues() {
        const values = {};
        const booleanValues = ['mailReminders', 'status'];
        const dropDownValues = ['title'];
        for (const key in this.refs) {
            values[key] = this.refs[key].getValue();
        }
        _.each(booleanValues, (booleanValue) => { values[booleanValue] = this.state[booleanValue]; });
        _.each(dropDownValues, (dropDownValue) => { values[dropDownValue] = this.state[dropDownValue]; });
        return values;
    }


    handleInput(e) {
        e.preventDefault;
        this.saveCustomer();
    }

    handleCheckBoxInput(name, e, value) {
        v = {};
        v[name] = value;
        this.setState(v, () => { this.saveCustomer(); });
    }

    handleSelectInput(name, e, key, payload) {
        v = {};
        v[name] = payload;
        this.setState(v, () => { this.saveCustomer(); });
    }

    saveCustomer() {
        const customer = this.getValues();
        const { customerId } = this.state;
        Meteor.call('saveCustomer', customerId, customer, (err, res) => {
          if (!err) {
            Bert.alert({
                title: 'All Changes Saved',
                type: 'success',
                style: 'growl-top-right',
                icon: 'fa-check'
            });
            if (!customerId) { this.setState({ customerId: res.insertedId }); }
          }
      });
    }


    render() {
        const { customer } = this.props;
        const styles = {
          block: {
            maxWidth: 250,
          },
          checkbox: {
            marginBottom: 16,
          },
        };

        return (
            <Row>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="customerName"
                        floatingLabelText="Customer name"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={customer && customer.customerName}
                        hintText="Customer Name"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="customerFirstName"
                        floatingLabelText="First Name"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={customer && customer.customerFirstName}
                        hintText="First Name"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="customerMiddleInitial"
                        floatingLabelText="Middle Initial"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={customer && customer.customerMiddleInitial}
                        hintText="Middle Initial"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="customerLastName"
                        floatingLabelText="Last Name"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={customer && customer.customerLastName}
                        hintText="Last Name"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="contactName"
                        floatingLabelText="Contact Name"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={customer && customer.contactName}
                        hintText="Contact Name"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="mainPhone"
                        floatingLabelText="Phone"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={customer && customer.mainPhone}
                        hintText="Phone"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="otherPhone"
                        floatingLabelText="Other Phone"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={customer && customer.otherPhone}
                        hintText="Other Phone"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="fax"
                        floatingLabelText="Fax"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={customer && customer.fax}
                        hintText="Fax"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="other"
                        floatingLabelText="Other Customer ID"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={customer && customer.other}
                        hintText="Other customer ID"
                    />
                </Col>
                <Col xs={12} md={4} sm={6}>
                    <TextField
                        ref="address1"
                        floatingLabelText="Address One"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={customer && customer.address1}
                        hintText="Address One"
                    />
                </Col>
                <Col xs={12} md={2} sm={6}>
                    <TextField
                        ref="address2"
                        floatingLabelText="Address two"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={customer && customer.address2}
                        hintText="Address two"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="city"
                        floatingLabelText="City"
                        fullWidth
                        maxLength="5"
                        type="number"
                        onChange={this.handleInput}
                        defaultValue={customer && customer.city}
                        hintText="City"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="state"
                        maxLength="2"
                        fullWidth
                        floatingLabelText="State"
                        onChange={this.handleInput}
                        defaultValue={customer && customer.state}
                        hintText="State"
                    />
                </Col>
                <Col xs={12} md={2} sm={6}>
                    <TextField
                        ref="zipCode"
                        floatingLabelText="Zip code"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={customer && customer.zipCode}
                        hintText="Zip code"
                    />
                </Col>
                <Col xs={12} md={2} sm={6}>
                    <Checkbox
                        label="Status"
                        onCheck={this.handleCheckBoxInput.bind(this, 'status')}
                        defaultChecked={customer && customer.status}
                        hintText="Status"
                    />
                </Col>
                <Col xs={12} md={2} sm={6}>
                    <SelectField
                        fullWidth
                        floatingLabelText="Title"
                        onChange={this.handleSelectInput.bind(this, 'title')}
                        value={customer && customer.title}
                        children={
                            Customers.schema.getAllowedValuesForKey('title').map(item => <MenuItem value={item} primaryText={item} />)
                        }
                        hintText="Title"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="email"
                        fullWidth
                        name="email"
                        floatingLabelText="Email"
                        onChange={this.handleInput}
                        defaultValue={customer && customer.email}
                        hintText="email"
                    />
                </Col>
                <Col xs={12} md={2} sm={3}>
                        <Checkbox
                        label="Mail Reminders"
                        onCheck={this.handleCheckBoxInput.bind(this, 'mailReminders')}
                        defaultChecked={customer && customer.mailReminders}
                        />
                </Col>
                <Col xs={12} md={12} sm={12}>
                    <TextField
                        ref="comment"
                        fullWidth
                        multiLine
                        rows={2}
                        floatingLabelText="Comment"
                        onChange={this.handleInput}
                        defaultValue={customer && customer.comment}
                        hintText="comment"
                    />
                </Col>
            </Row>
        );
    }
}

CustomerEditor.propTypes = {
  customer: PropTypes.object,
};
