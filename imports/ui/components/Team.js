import React from 'react';
import { browserHistory } from 'react-router';
import { Glyphicon, Col, Row } from 'react-bootstrap';
import { Bert } from 'meteor/themeteorchef:bert';
import { removeTeam } from '/imports/api/teams/methods';

export class Team extends React.Component {
  constructor(props) {
      super(props);
      this.handleShow.bind(this);
  }
  handleShow() {
    const team = this.props.team;

    browserHistory.push(`/teams/${team._id}`);
  }

  handleEdit() {
    const team = this.props.team;

    browserHistory.push(`/teams/${team._id}`);
  }

  handleRemove() {
    const team = this.props.team;

    if (confirm('Are you sure? This is permanent.')) {
      removeTeam.call(team._id, (error) => {
        if (error) {
          Bert.alert(error.reason, 'danger');
        } else {
          Bert.alert('Team removed!', 'success');
        }
      });
    }
  }

  render() {
    const team = this.props.team;
    const style = {
      cursor: 'pointer'
    };

    return (
        <div className="widget stats-widget" style={style} onClick={this.handleShow.bind(this)}>
          <div className="widget-body clearfix">
            <div className="pull-left">
              <h3 className="widget-title text-primary">
                <span className="counter" data-plugin="counterUp">{team.name}</span>
              </h3>
              <small className="text-color">{team.contacts ? `${team.contacts.length} Members` : null}</small>
            </div>
          </div>
          <footer className="widget-footer bg-primary">
            <small>10 tasks posted</small> <br />
            <small>3 active tasks</small> 
            <span 
              className="small-chart pull-right" 
              data-plugin="sparkline" 
              data-options="
              [4,3,5,2,1], { type: 'bar', barColor: '#ffffff', barWidth: 5, barSpacing: 2 }"
            >
              <canvas 
                width={33} 
                height={16} 
                style={{
                  display: 'inline-block', width: 33, height: 16, verticalAlign: 'top'
                }} 
              />
            </span>
          </footer>
        </div>
    );
  }
}

Team.propTypes = { team: React.PropTypes.object };
