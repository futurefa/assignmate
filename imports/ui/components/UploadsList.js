import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Meteor } from 'meteor/meteor';
import Subheader from 'material-ui/Subheader';
import Avatar from 'material-ui/Avatar';
import { List, ListItem } from 'material-ui/List';
import ActionAssignment from 'material-ui/svg-icons/action/assignment';
import FileCloudUpload from 'material-ui/svg-icons/file/cloud-upload';
import FileCloudDownload from 'material-ui/svg-icons/file/cloud-download';
import ActionInfo from 'material-ui/svg-icons/action/info';
import ActionDelete from 'material-ui/svg-icons/action/delete';
import { blue500, yellow600 } from 'material-ui/styles/colors';
import { ListGroup, ListGroupItem, Alert } from 'react-bootstrap';

import Files from '../../api/files/files';
import { removeFile } from '../../api/files/methods';
import container from '../../modules/container';

const handleRemove = (_id) => {
    if (confirm('Are you sure? This is permanent!')) {
        Meteor.call('removeFile', _id, (error, res) => {
            if (error) {
                Bert.alert(error.reason, 'danger');
            } else {
                Bert.alert('File deleted!', 'success');
                // browserHistory.push('/tasks');
            }
        });
    }
};

const downloadFile = (file) => {
    let win; 
    win = window.open(file.url(), '_blank');
    win.focus();
};


/* const UploadsList = ({ files }) => (
    files.length > 0 ?
    <List>
      <Subheader inset={true}>Files</Subheader>
        {files.map((file)=>{
            <ListItem
              leftAvatar={<Avatar icon={<ActionAssignment />} backgroundColor={blue500} />}
              rightIcon={<ActionInfo />}
              primaryText={file.original.name}
              secondaryText="Jan 20, 2014"
            />
        })}
    </List> :
   <Alert bsStyle="warning">No files yet.</Alert>
 );*/

const UploadsList = ({ files }) => (
  files.length > 0 ? <List className="DocumentsList">
    {/*<ListItem
              leftAvatar={<Avatar icon={<FileCloudUpload />} backgroundColor={'grey'} />}
              primaryText={''}
              secondaryText={'Click to upload new file'}
            />*/}
    {files.map((file) => (
      <ListItem
              onClick={() => {
                downloadFile(file);
              }}
              key={file._id}
              leftAvatar={<Avatar icon={<ActionAssignment />} backgroundColor={yellow600} />}
              rightIcon={<ActionDelete
onClick={(event) => {
                event.stopPropagation();
                handleRemove(file._id);
              }}
              />}
              primaryText={file.name()}
              secondaryText={`${file.size() / 1000}KB`}
      />
    ))}
  </List> :
  <Alert bsStyle="warning">No files yet.</Alert>
);

UploadsList.propTypes = {
    files: PropTypes.array,
};

export default container((props, onData) => {
    const subscription = Meteor.subscribe('files.list');
    if (subscription.ready()) {
        const selector = { itemId: props.item && props.item._id };
        const files = Files.find(selector).fetch();
        
        onData(null, { files });
    }
}, UploadsList);
