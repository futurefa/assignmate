import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Button, ButtonGroup, Alert } from 'react-bootstrap';
import { Meteor } from 'meteor/meteor';
import Devices from '../../api/devices/devices';
import { removeDevice } from '../../api/devices/methods';
import container from '../../modules/container';
import Griddle from 'griddle-react';
import RaisedButton from 'material-ui/RaisedButton';

const moment = require('moment');

const handleNav = (_id, datas) => console.log(_id);

const handleRemove = (_id) => {
    if (confirm('Are you sure? This is permanent!')) {
        Meteor.call('removeDevice', _id, (error) => {
            if (error) {
                Bert.alert(error.reason, 'danger');
            } else {
                Bert.alert('Device deleted!', 'success');
                browserHistory.push('/devices');
            }
        });
    }
};

const style = {
    marginRight: 10
};

const actionButtons = React.createClass({
  render() {
    return (
        <div>
            <RaisedButton
                label="Edit"
                style={style}
                onClick={() => browserHistory.push(`/devices/${this.props.rowData._id}`)}
            />
            <RaisedButton
                label="Delete"
                primary
                onClick={() => handleRemove(this.props.rowData._id)}
            />
        </div>
    );
  }
});

const testMonthComponent = React.createClass({
  render() {
    const date = moment(this.props.rowData.testMonth).format('YYYY-MM-DD');
    return (
        <div>
            {date}
        </div>
    );
  }
});
const installDateComponent = React.createClass({
  render() {
    const date = moment(this.props.rowData.installDate).format('YYYY-MM-DD');
    return (
        <div>
            {date}
        </div>
    );
  }
});

const nextTestDueComponent = React.createClass({
    render() {
        const date = moment(this.props.rowData.nextTestDue).format('YYYY-MM-DD');
        return (
            <div>
                {date}
            </div>
        );
    }
});

const columnList = [
    'make', 'model', 'size', 'type', 'serialNumber', 'deviceLocation',
    'active', 'nextTestDue', 'actions'
];
const columnMetadata = [
    { columnName: 'make', order: '1', displayName: 'Device Name', cssClassName: 'col-xs-2' },
    { columnName: 'model', order: '2', displayName: 'Model', cssClassName: 'col-xs-2' },
    { columnName: 'size', order: '3', displayName: 'Size', cssClassName: 'col-xs-2' },
    { columnName: 'type', order: '4', displayName: 'Type', cssClassName: 'col-xs-1' },
    { columnName: 'serialNumber', order: '5', displayName: 'Serial Number', cssClassName: 'col-xs-2' },
    { columnName: 'deviceLocation', order: '6', displayName: 'Device Location', cssClassName: 'col-xs-2' },
    { columnName: 'active', order: '7', displayName: 'Active', cssClassName: 'col-xs-1' },
    { columnName: 'nextTestDue', order: '8', customComponent: nextTestDueComponent, displayName: 'Test Due', cssClassName: 'col-xs-2' },
    { columnName: 'actions',
customComponent: actionButtons,
sortable: false,
cssClassName: 'col-xs-2',
    displayName: 'Actions' }
];

const DevicesList = ({ devices }) => (
devices.length > 0 ?
    <Griddle
        results={devices}
        useGriddleStyles={false}
        tableClassName={'griddle-flex table table-bordered table-striped table-hover highlight striped bordered'}
        settingsToggleClassName='btn btn-default'
        useCustomPagerComponent={false}
        showFilter
        showSettings
        resultsPerPage={50}
        columnMetadata={columnMetadata}
        columns={columnList}
    /> :
   <Alert bsStyle="warning">No devices yet.</Alert>
);

DevicesList.propTypes = {
    devices: PropTypes.array,
};

export default container((props, onData) => {
    const subscription = Meteor.subscribe('devices.list');
    if (subscription.ready()) {
        const devices = Devices.find().fetch();
        onData(null, { devices });
    }
}, DevicesList);
