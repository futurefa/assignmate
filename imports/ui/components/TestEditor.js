
import React from 'react';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';
import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import DatePicker from 'material-ui/DatePicker';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import Testings from '../../api/testings/testings';
import _ from 'lodash';

const moment = require('moment');


export default class TestEditor extends React.Component {
    constructor(props) {
        super(props);
        this.handleInput = _.debounce(this.handleInput.bind(this), 2500);
        this.state = {
            testId: this.props.test ? this.props.test._id : '',
            month: this.props.test ? this.props.test.month : new Date(),
            testDate: this.props.test ? this.props.test.testDate : new Date(),
            nextDate: this.props.test ? this.props.test.nextDate : new Date(),
            calibrationDate: this.props.test ? this.props.test.calibrationDate : new Date(),
            timeIn: this.props.test ? this.props.test.timeIn : new Date(),
            timeOut: this.props.test ? this.props.test.timeOut : new Date(),
            sent: this.props.test ? this.props.test.sent : new Date(),
        };
    }

    getValues() {
        const values = {};
        const booleanValues = ['serviceRestored', 'airGapOk', 'printed'];
        const dateValues = ['month', 'testDate', 'nextDate', 'calibrationDate', 'timeIn', 'timeOut', 'sent'];
        const dropDownValues = ['pressureUnit', 'meterReading', 'status'];
        for (const key in this.refs) {
            values[key] = this.refs[key].getValue();
        }
        _.each(dateValues, (dateValue) => { values[dateValue] = new Date(this.state[dateValue]); });
        _.each(booleanValues, (booleanValue) => { values[booleanValue] = this.state[booleanValue]; });
        _.each(dropDownValues, (dropDownValue) => { values[dropDownValue] = this.state[dropDownValue]; });
        return values;
    }

    handleDateInput(name, e, date) {
        v = {};
        v[name] = moment(date).format('YYYY-MM-DD');
        this.setState(v, () => { this.saveTest(); });
    }

    handleCheckBoxInput(name, e, value) {
        v = {};
        v[name] = value;
        this.setState(v, () => { this.saveTest(); });
    }

    handleSelectInput(name, e, key, payload) {
        v = {};
        v[name] = payload;
        this.setState(v, () => { this.saveTest(); });
    }

    handleInput(e) {
        e.preventDefault;
        this.saveTest();
    }

    saveTest() {
        const test = this.getValues();
        const { testId } = this.state;
        test.installId = this.props.device ? this.props.device._id : this.props.test.installId;
        Meteor.call('saveTest', testId, test, (err, res) => {
            if (!err) {
              Bert.alert({
                  title: 'All Changes Saved',
                  type: 'success',
                  style: 'growl-top-right',
                  icon: 'fa-check'
              });
              if (!testId) { this.setState({ testId: res.insertedId }); }
            } else {
                console.log(err);
            }
        });
    }


    render() {
        const { test, device } = this.props;
        const styles = {
          block: {
            maxWidth: 250,
          },
          checkbox: {
            marginBottom: 16,
          },
        };

        return (
            <Row>
                <Col xs={12} md={3} sm={6}>
                    <DatePicker
                        name="month"
                        floatingLabelText="Test Month"
                        onChange={this.handleDateInput.bind(this, 'month')}
                        defaultDate={this.state.month}
                        hintText="Test Month"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="type"
                        floatingLabelText="Type"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={test && test.type}
                        hintText="Type"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <SelectField
                        fullWidth
                        floatingLabelText="Pressure Unit"
                        onChange={this.handleSelectInput.bind(this, 'pressureUnit')}
                        value={test && test.pressureUnit}
                        children={
                            Testings.schema.getAllowedValuesForKey('pressureUnit').map(item => <MenuItem value={item} primaryText={item} />)
                        }
                        hintText="Pressure Unit"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="linePressure"
                        floatingLabelText="Line Pressure"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={test && test.linePressure}
                        hintText="Line Pressure"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="meterReading"
                        floatingLabelText="Meter reading"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={test && test.meterReading}
                        hintText="Meter reading"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="repairSummary"
                        floatingLabelText="Repair summary"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={test && test.repairSummary}
                        hintText="Repair summary"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <Checkbox
                        label="Service restored"
                        onCheck={this.handleCheckBoxInput.bind(this, 'serviceRestored')}
                        defaultChecked={test && test.serviceRestored}
                    />
                </Col>
                <Col xs={12} md={4} sm={6}>
                    <Checkbox
                        label="Air gap Ok"
                        onCheck={this.handleCheckBoxInput.bind(this, 'airGapOk')}
                        defaultChecked={test && test.airGapOk}
                    />
                </Col>
                <Col xs={12} md={2} sm={6}>
                    <DatePicker
                        name="testDate"
                        floatingLabelText="Test Date"
                        onChange={this.handleDateInput.bind(this, 'testDate')}
                        defaultDate={this.state.testDate}
                        hintText="Test Date"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <SelectField
                        fullWidth
                        floatingLabelText="Status"
                        onChange={this.handleSelectInput.bind(this, 'status')}
                        value={test && test.status}
                        children={
                            Testings.schema.getAllowedValuesForKey('status').map(item => <MenuItem value={item} primaryText={item} />)
                        }
                        hintText="Status"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <DatePicker
                        name="nextDate"
                        floatingLabelText="Next Test Date"
                        onChange={this.handleDateInput.bind(this, 'nextDate')}
                        defaultDate={this.state.nextDate}
                        hintText="Next Test Date"
                    />
                </Col>
                <Col xs={12} md={2} sm={6}>
                    <TextField
                        ref="certNo"
                        floatingLabelText="Certificate number"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={test && test.certNo}
                        hintText="Certificate Number"
                    />
                </Col>
                <Col xs={12} md={2} sm={6}>
                    <TextField
                        ref="testerName"
                        floatingLabelText="Tester's name"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={test && test.testerName}
                        hintText="Tester's name"
                    />
                </Col>
                <Col xs={12} md={2} sm={6}>

                    <TextField
                        ref="gaugeNo"
                        floatingLabelText="Gauge no"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={test && test.gaugeNo}
                        hintText="Test kit gauge no"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <DatePicker
                        name="calibrationDate"
                        floatingLabelText="Calibration date"
                        onChange={this.handleDateInput.bind(this, 'calibrationDate')}
                        defaultDate={this.state.calibrationDate}
                        hintText="Calibration date"
                    />
                </Col>
                <Col xs={12} md={2} sm={3}>
                    <DatePicker
                        name="timeIn"
                        floatingLabelText="Time in"
                        onChange={this.handleDateInput.bind(this, 'timeIn')}
                        defaultDate={this.state.timeIn}
                        hintText="Time in"
                    />
                </Col>
                <Col xs={12} md={2} sm={3}>
                    <DatePicker
                        name="timeOut"
                        floatingLabelText="Time out"
                        onChange={this.handleDateInput.bind(this, 'timeOut')}
                        defaultDate={this.state.timeOut}
                        hintText="Time out"
                    />
                </Col>
                <Col xs={12} md={2} sm={3}>
                    <DatePicker
                        name="sent"
                        floatingLabelText="Date test sent"
                        onChange={this.handleDateInput.bind(this, 'sent')}
                        defaultDate={this.state.sent}
                        hintText="Date test sent"
                    />
                </Col>
                <Col xs={12} md={4} sm={6}>
                        <Checkbox
                        label="Test printed"
                        onCheck={this.handleCheckBoxInput.bind(this, 'printed')}
                        defaultChecked={test && test.printed}
                        />
                </Col>
                <Col xs={12} md={12} sm={12}>
                    <TextField
                        ref="remarks"
                        fullWidth
                        floatingLabelText="Remarks"
                        onChange={this.handleInput}
                        defaultValue={test && test.remarks}
                        hintText="Remarks"
                    />
                </Col>
                <Col xs={12} md={12} sm={12}>
                    <TextField
                        ref="comments"
                        fullWidth
                        floatingLabelText="Comments"
                        onChange={this.handleInput}
                        defaultValue={test && test.comments}
                        hintText="comments"
                    />
                </Col>
            </Row>
        );
    }
}

TestEditor.propTypes = {
  test: PropTypes.object,
  device: PropTypes.object
};
