import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory, Link } from 'react-router';
import { Alert } from 'react-bootstrap';
import { Meteor } from 'meteor/meteor';
import RaisedButton from 'material-ui/RaisedButton';
import moment from 'moment';

import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Tasks from '../../api/tasks/tasks';
import { removeTask } from '../../api/tasks/methods';
import container from '../../modules/container';

const handleRemove = (_id) => {
    if (confirm('Are you sure? This is permanent!')) {
        Meteor.call('removeTask', _id, (error, res) => {
            if (error) {
                Bert.alert(error.reason, 'danger');
            } else {
                Bert.alert('Task deleted!', 'success');
                browserHistory.push('/tasks');
            }
        });
    }
};

const displayData = function (dataArray) {
    return dataArray.map((data) => (
            <TableRow key={data._id}>
            <TableRowColumn>{data.title}</TableRowColumn>
            <TableRowColumn>{data.dueDate ? moment(data.dueDate).fromNow() : '-'}</TableRowColumn>
            <TableRowColumn>{data.owner && data.owner.name}</TableRowColumn>
            <TableRowColumn>
                <Link to={`/tasks/${data._id}`}><RaisedButton label="View" primary />
                </Link>
                <Link>
                    <RaisedButton
                        onTouchTap={() => handleRemove(data._id)}
                        label="Delete"
                        secondary 
                    />
                </Link>
            </TableRowColumn>
            </TableRow>
        ));
};

 const TasksList = ({ tasks }) => (
    tasks.length > 0 ?
        <MuiThemeProvider>
            <Table>
            <TableHeader>
              <TableRow>
                <TableHeaderColumn>Title</TableHeaderColumn>
                <TableHeaderColumn>Due</TableHeaderColumn>
                <TableHeaderColumn>Created By</TableHeaderColumn>
                <TableHeaderColumn>Action</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody>
            {displayData(tasks)}
            </TableBody>
          </Table>
        </MuiThemeProvider> :
   <Alert bsStyle="warning">No tasks yet.</Alert>
 );

TasksList.propTypes = {
    tasks: PropTypes.array,
    team: PropTypes.object,
};

export default container((props, onData) => {
    const subscription = Meteor.subscribe('tasks.list');
    if (subscription.ready()) {
        const tasks = props.team && props.team._id ? Tasks.find({ 'team._id': props.team._id }).fetch() : Tasks.find().fetch();
        onData(null, { tasks });
    }
}, TasksList);
