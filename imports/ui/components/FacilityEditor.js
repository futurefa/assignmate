import React from 'react';
import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import PropTypes from 'prop-types';
import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Checkbox from 'material-ui/Checkbox';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import Facilities from '../../api/facilities/facilities';
import _ from 'lodash';
import Griddle from 'griddle-react';
import ContactEditor from './ContactEditor';


export default class FacilityEditor extends React.Component {
    constructor(props) {
        super(props);
        this.handleInput = _.debounce(this.handleInput.bind(this), 2500);
        this.state = {
            facilityId: this.props.facility ? this.props.facility._id : '',
            nextSurvey: this.props.facility ? this.props.facility.nextSurvey : null,
            testMonth: this.props.facility ? this.props.facility.testMonth : null,
            contacts: this.props.facility ? (this.props.facility.contacts ? this.props.facility.contacts : []) : [],
            currentContact: undefined,
            salutation: this.props.facility ? this.props.facility.salutation : undefined,
            facilityType: this.props.facility ? this.props.facility.facilityType : undefined,
            'additionalInformation.customerSalutation': this.props.facility ?
                (this.props.facility.additionalInformation ? this.props.facility.additionalInformation.customerSalutation : undefined) : undefined
        };
        this.setContact = this.setContact.bind(this);
    }

    getValues() {
        const values = {};
        const booleanValues = ['active'];
        const dateValues = ['nextSurvey', 'testMonth'];
        const dropDownValues = ['facilityType', 'salutation', 'additionalInformation.customerSalutation'];
        for (const key in this.refs) {
            values[key] = this.refs[key].getValue();
        }
        values.contacts = this.state.contacts;
        _.each(booleanValues, (booleanValue) => { values[booleanValue] = this.state[booleanValue]; });
        _.each(dateValues, (dateValue) => { values[dateValue] = this.state[dateValue]; });
        _.each(dropDownValues, (dropDownValue) => { values[dropDownValue] = this.state[dropDownValue]; });
        return values;
    }

    setContact(contact) {
        this.setState({ currentContact: contact });
    }

    updateContacts() {
        const contacts = this.state.contacts || [];
        const contact = this.state.currentContact;
        if (!contact) { return false; }
        if (contact.contactId) {
            const index = _.findIndex(contacts, _.pick(contact, 'contactId'));
            contacts.splice(index, 1, contact);
        } else {
            contact.contactId = Random.id();
            contacts.push(contact);
        }
        this.setState({ contacts }, () => {
            this.saveFacility();
        });
    }

    handleDateInput(name, e, date) {
        v = {};
        v[name] = date;
        this.setState(v, () => {
            this.saveFacility();
        });
    }
    handleSelectInput(name, e, key, payload) {
        v = {};
        v[name] = payload;
        this.setState(v, () => { this.saveFacility(); });
    }

    handleCheckBoxInput(name, e, value) {
        v = {};
        v[name] = value;
        this.setState(v, () => { this.saveFacility(); });
    }

    handleInput(e) {
        e.preventDefault;
        this.saveFacility();
    }

    saveFacility() {
        const facility = this.getValues();
        facility.customerID = this.props.getCustomer() ? this.props.getCustomer()._id : null;
        const { facilityId } = this.state;
        Meteor.call('saveFacility', facilityId, facility, (err, res) => {
            if (!err) {
                Bert.alert({
                    title: 'All Changes Saved',
                    type: 'success',
                    style: 'growl-top-right',
                    icon: 'fa-check'
                });
                if (!facilityId) { this.setState({ facilityId: res.insertedId }); }
            } else {

            }
        });
    }

    handleOpen() {
        this.setState({ currentContact: {} });
        this.setState({ open: true });
    }

    handleClose() {
        this.setState({ open: false });
        this.updateContacts();
    }

    componentDidUpdate() {
        // this.saveFacility();
    }

    editContact(contact) {
        this.setState({ currentContact: contact });
        this.setState({ open: true });
    }

    removeContact(contactId) {
        const contacts = _.remove(this.state.contacts, (contact) => contact.contactId != contactId);
        this.setState({ contacts }, () => {
            this.saveFacility();
        });
    }


    render() {
        const self = this;
        const { facility } = this.props;
        const columnList = [
            'contactName', 'type', 'mailingName', 'address1',
            'city', 'state', 'email', 'active', 'phone', 'actions'
        ];
        const actionButtons = React.createClass({
            render() {
                return (
                    <div>
                        <RaisedButton
                            label="Edit"
                            style={{ marginRight: 10 }}
                            onClick={() => self.editContact(this.props.rowData)}
                        />
                        <RaisedButton
                            label="Delete"
                            primary
                            onClick={() => self.removeContact(this.props.rowData.contactId)}
                        />
                    </div>
                );
            }
        });

        const actions = [
            <FlatButton
                label="Cancel"
                primary
                onTouchTap={this.handleClose.bind(this)}
            />,
            <FlatButton
                label="Save"
                primary
                keyboardFocused
                onTouchTap={this.handleClose.bind(this)}
            />,
        ];

        const panelStyle = {
            border: '1px solid #ccc',
            borderRadius: '5px',
            padding: '10px'
        };

        const columnMetadata = [
            { columnName: 'contactName', order: '1', displayName: 'Contact Name', cssClassName: 'col-xs-2' },
            { columnName: 'type', order: '2', displayName: 'Type', cssClassName: 'col-xs-2' },
            { columnName: 'mailingName', order: '3', displayName: 'Mailing Name', cssClassName: 'col-xs-2' },
            { columnName: 'address1', order: '4', displayName: 'Address One', cssClassName: 'col-xs-2' },
            { columnName: 'city', order: '5', displayName: 'City', cssClassName: 'col-xs-2' },
            { columnName: 'state', order: '6', displayName: 'State', cssClassName: 'col-xs-2' },
            { columnName: 'email', order: '7', displayName: 'Email', cssClassName: 'col-xs-2' },
            { columnName: 'phone', order: '8', displayName: 'Phone', cssClassName: 'col-xs-2' },
            { columnName: 'active', order: '9', displayName: 'Active', cssClassName: 'col-xs-2' },
            { columnName: 'actions', customComponent: actionButtons, displayName: 'Actions', sortable: false, cssClassName: 'col-xs-2' },
        ];

        return (
            <Row>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="facilityName"
                        floatingLabelText="Facility name"
                        onChange={this.handleInput}
                        defaultValue={facility && facility.facilityName}
                        fullWidth
                        hintText="Facility Name"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="storeNumber"
                        floatingLabelText="Store Number"
                        onChange={this.handleInput}
                        defaultValue={facility && facility.storeNumber}
                        fullWidth
                        hintText="Store Number"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="accountNumber"
                        floatingLabelText="Account Number"
                        onChange={this.handleInput}
                        defaultValue={facility && facility.accountNumber}
                        fullWidth
                        hintText="Account Number"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                        <SelectField
                        fullWidth
                        floatingLabelText="Facility Type"
                        onChange={this.handleSelectInput.bind(this, 'facilityType')}
                        value={this.state.facilityType}
                        children={
                            Facilities.schema.getAllowedValuesForKey('facilityType').map(item => <MenuItem value={item} primaryText={item} />)
                        }
                        hintText="Facility Type"
                        />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="hazardLevel"
                        floatingLabelText="Hazard Level"
                        onChange={this.handleInput}
                        defaultValue={facility && facility.hazardLevel}
                        fullWidth
                        hintText="Hazard Level"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <DatePicker
                        name="testMonth"
                        floatingLabelText="Test Month"
                        onChange={this.handleDateInput.bind(this, 'testMonth')}
                        value={this.state.testMonth}
                        fullWidth
                        hintText="Test Month"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="surveyCycle"
                        floatingLabelText="Survey Cycle"
                        onChange={this.handleInput}
                        defaultValue={facility && facility.surveyCycle}
                        fullWidth
                        hintText="Survey Cycle"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="address1"
                        floatingLabelText="Address One"
                        onChange={this.handleInput}
                        defaultValue={facility && facility.address1}
                        fullWidth
                        hintText="Address One"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="address2"
                        floatingLabelText="Address two"
                        onChange={this.handleInput}
                        defaultValue={facility && facility.address2}
                        fullWidth
                        hintText="Address two"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="streetName"
                        maxLength="2"
                        floatingLabelText="Street Name"
                        onChange={this.handleInput}
                        defaultValue={facility && facility.streetName}
                        fullWidth
                        hintText="Street Name"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="city"
                        floatingLabelText="City"
                        maxLength="5"
                        type="number"
                        onChange={this.handleInput}
                        defaultValue={facility && facility.city}
                        fullWidth
                        hintText="City"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="state"
                        maxLength="2"
                        floatingLabelText="State"
                        onChange={this.handleInput}
                        defaultValue={facility && facility.state}
                        fullWidth
                        hintText="State"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="zipCode"
                        floatingLabelText="Zip code"
                        onChange={this.handleInput}
                        defaultValue={facility && facility.zipCode}
                        fullWidth
                        hintText="Zip code"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <DatePicker
                        floatingLabelText="Next Survey"
                        onChange={this.handleDateInput.bind(this, 'nextSurvey')}
                        value={this.state.nextSurvey}
                        fullWidth
                        hintText="Next Survey"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="propertyIDNumber"
                        name="propertyIDNumber"
                        floatingLabelText="Property Id Number"
                        onChange={this.handleInput}
                        defaultValue={facility && facility.propertyIDNumber}
                        fullWidth
                        hintText="Property Id Number"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="streetNumber"
                        name="streetNumber"
                        floatingLabelText="Street Number"
                        onChange={this.handleInput}
                        defaultValue={facility && facility.streetNumber}
                        fullWidth
                        hintText="Street Number"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="contactName"
                        name="contactName"
                        floatingLabelText="Contact name"
                        onChange={this.handleInput}
                        defaultValue={facility && facility.contactName}
                        fullWidth
                        hintText="Contact name"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="phone"
                        name="phone"
                        floatingLabelText="Phone"
                        onChange={this.handleInput}
                        defaultValue={facility && facility.phone}
                        fullWidth
                        hintText="Phone"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="fax"
                        name="fax"
                        floatingLabelText="Fax"
                        onChange={this.handleInput}
                        defaultValue={facility && facility.fax}
                        fullWidth
                        hintText="Fax"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="otherPhone"
                        name="otherPhone"
                        floatingLabelText="Other phone"
                        onChange={this.handleInput}
                        defaultValue={facility && facility.otherPhone}
                        fullWidth
                        hintText="Other phone"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="email"
                        name="email"
                        floatingLabelText="email"
                        onChange={this.handleInput}
                        defaultValue={facility && facility.email}
                        fullWidth
                        hintText="email"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="noOfUnits"
                        name="noOfUnits"
                        floatingLabelText="no of units"
                        onChange={this.handleInput}
                        defaultValue={facility && facility.noOfUnits}
                        fullWidth
                        hintText="no of units"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="mapCode"
                        name="mapCode"
                        floatingLabelText="Map code"
                        onChange={this.handleInput}
                        defaultValue={facility && facility.mapCode}
                        fullWidth
                        hintText="Map code"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                        <SelectField
                        fullWidth
                        floatingLabelText="Salutation"
                        onChange={this.handleSelectInput.bind(this, 'salutation')}
                        value={this.state.salutation}
                        children={
                            Facilities.schema.getAllowedValuesForKey('salutation').map(item => <MenuItem value={item} primaryText={item} />)
                        }
                        hintText="Salutation"
                        />
                </Col>
                <Col xs={12} md={12} sm={12} style={panelStyle}>
                    <h3>Additional Information</h3>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="additionalInformation.contactName"
                            floatingLabelText="Contact name"
                            onChange={this.handleInput}
                            defaultValue={facility && facility.additionalInformation && facility.additionalInformation.contactName}
                            fullWidth
                            hintText="Contact name"
                        />
                    </Col>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="additionalInformation.phone"
                            floatingLabelText="Phone"
                            onChange={this.handleInput}
                            defaultValue={facility && facility.additionalInformation && facility.additionalInformation.phone}
                            fullWidth
                            hintText="Phone"
                        />
                    </Col>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="additionalInformation.fax"
                            floatingLabelText="Fax"
                            onChange={this.handleInput}
                            defaultValue={facility && facility.additionalInformation && facility.additionalInformation.fax}
                            fullWidth
                            hintText="Fax"
                        />
                    </Col>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="additionalInformation.otherPhone"
                            floatingLabelText="Other Phone"
                            onChange={this.handleInput}
                            defaultValue={facility && facility.additionalInformation && facility.additionalInformation.otherPhone}
                            fullWidth
                            hintText="Other Phone"
                        />
                    </Col>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="additionalInformation.email"
                            floatingLabelText="email"
                            onChange={this.handleInput}
                            defaultValue={facility && facility.additionalInformation && facility.additionalInformation.email}
                            fullWidth
                            hintText="Email"
                        />
                    </Col>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="additionalInformation.noOfUnits"
                            floatingLabelText="No Of Units"
                            onChange={this.handleInput}
                            defaultValue={facility && facility.additionalInformation && facility.additionalInformation.noOfUnits}
                            fullWidth
                            hintText="No of units"
                        />
                    </Col>
                    <Col xs={12} md={3} sm={6}>
                        <TextField
                            ref="additionalInformation.mapCode"
                            floatingLabelText="Map Code"
                            onChange={this.handleInput}
                            defaultValue={facility && facility.additionalInformation && facility.additionalInformation.mapCode}
                            fullWidth
                            hintText="Map Code"
                        />
                    </Col>
                    <Col xs={12} md={3} sm={6}>
                        <SelectField
                            floatingLabelText="Salutation"
                            fullWidth
                            onChange={this.handleSelectInput.bind(this, 'additionalInformation.customerSalutation')}
                            value={this.state['additionalInformation.customerSalutation']}
                            children={
                                Facilities.schema.getAllowedValuesForKey('salutation').map(item => <MenuItem value={item} primaryText={item} />)
                            }
                            hintText="Salutation"
                        />
                        {/* <TextField
                            ref="additionalInformation.customerSalutation"
                            floatingLabelText="Customer Salutation"
                            onChange={ this.handleInput }
                            defaultValue={ facility && facility.additionalInformation && facility.additionalInformation.customerSalutation }
                            fullWidth={true}
                            hintText="Customer Salutation"/> */}
                    </Col>
                </Col>
                <Col xs={12} md={12} sm={12}>
                    <TextField
                        ref="comment"
                        multiLine
                        rows={2}
                        floatingLabelText="Comment"
                        onChange={this.handleInput}
                        defaultValue={facility && facility.comment}
                        fullWidth
                        hintText="comment"
                    />
                </Col>
                <Col xs={12} md={12} sm={12}>
                    <Checkbox
                        label="Active"
                        onCheck={this.handleCheckBoxInput.bind(this, 'active')}
                        defaultChecked={facility && facility.active}
                    />
                </Col>
                <Col xs={12} md={12} sm={12}>
                    <Col xs={12} sm={12} md={12}>
                        <RaisedButton label="Create New Contact" primary onTouchTap={this.handleOpen.bind(this)} />
                    </Col>
                    <Col xs={12} sm={12} md={12}>
                        {this.state.contacts.length ?
                            <Griddle
                                results={this.state.contacts}
                                useGriddleStyles={false}
                                tableClassName={'griddle-flex table table-bordered table-striped table-hover highlight striped bordered'}
                                settingsToggleClassName='btn btn-default'
                                useCustomPagerComponent={false}
                                showFilter
                                showSettings
                                resultsPerPage={50}
                                columnMetadata={columnMetadata}
                                columns={columnList}
                            /> :
                                <div />
                            }
                    </Col>
                </Col>
                <Dialog
                    title="Create New Contact"
                    actions={actions}
                    modal={false}
                    open={this.state.open}
                    onRequestClose={this.handleClose}
                    autoScrollBodyContent
                >
                    <Row>
                        <Col xs={12}>
                            <ContactEditor contact={this.state.currentContact} setContact={this.setContact} />
                        </Col>
                    </Row>
                </Dialog>
            </Row>
        );
    }
}

FacilityEditor.propTypes = {
    facility: PropTypes.object,
    getCustomer: PropTypes.func
};
