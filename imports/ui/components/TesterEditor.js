import React from 'react';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';
import { Col, Row } from 'meteor/jimmiebtlr:react-flexbox-grid';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import Checkbox from 'material-ui/Checkbox';


export default class TesterEditor extends React.Component {
    constructor(props) {
        super(props);
        this.handleInput = this.handleInput.bind(this);
        this.state = {
            testerId: this.props.tester ? this.props.tester._id : '',
            calibratedExp: this.props.tester ? this.props.tester.calibratedExp : null,
            certExp: this.props.tester ? this.props.tester.certExp : null
        };
    }

    getValues() {
        const values = {};
        const booleanValues = ['active', 'installer', 'inspector', 'tester'];
        const dateValues = ['certExp', 'calibrationExp'];
        for (const key in this.refs) {
            values[key] = this.refs[key].getValue();
        }

        _.each(booleanValues, (booleanValue) => { values[booleanValue] = this.state[booleanValue]; });
        _.each(dateValues, (dateValue) => { values[dateValue] = this.state[dateValue]; });
        return values;
    }

    handleDateInput(name, e, date) {
        v = {};
        v[name] = date;
        this.setState(v, () => {
            this.saveTester();
        });
    }

    handleCheckBoxInput(name, e, value) {
        v = {};
        v[name] = value;
        this.setState(v, () => { this.saveTester(); });
    }

    handleInput(e) {
        e.preventDefault;
        this.saveTester();
    }

    saveTester() {
        const tester = this.getValues();
        const { testerId } = this.state;
        Meteor.call('saveTester', testerId, tester, (err, res) => {
            if (!err) {
                _.debounce(
                    Bert.alert({
                        title: 'All Changes Saved',
                        type: 'success',
                        style: 'growl-top-right',
                        icon: 'fa-check'
                    }), 1000);
                if (!testerId) { this.setState({ testerId: res.insertedId }); }
            }
        });
    }

    componentWillUnmount() {
        _.debounce(
                    Bert.alert({
                        title: 'All Changes Saved',
                        type: 'success',
                        style: 'growl-top-right',
                        icon: 'fa-check'
                    }), 1000);
    }


    render() {
        const { tester } = this.props;
        const styles = {
          block: {
            maxWidth: 250,
          },
          checkbox: {
            marginBottom: 16,
          },
        };

        return (
            <Row>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="certNumber"
                        floatingLabelText="Certificate number"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={tester && tester.testerName}
                        hintText="Certificate Number"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="name"
                        floatingLabelText="Name"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={tester && tester.name}
                        hintText="Name"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="companyName"
                        floatingLabelText="Company Name"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={tester && tester.companyName}
                        hintText="Company Name"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="mainPhone"
                        floatingLabelText="phone"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={tester && tester.mainPhone}
                        hintText="Phone"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="otherPhone"
                        floatingLabelText="Other phone"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={tester && tester.otherPhone}
                        hintText="Other Phone"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="fax"
                        floatingLabelText="Fax"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={tester && tester.fax}
                        hintText="Fax"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="email"
                        fullWidth
                        name="email"
                        floatingLabelText="Email"
                        onChange={this.handleInput}
                        defaultValue={tester && tester.email}
                        hintText="email"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="address1"
                        floatingLabelText="Address One"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={tester && tester.address1}
                        hintText="Address One"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="address2"
                        floatingLabelText="Address two"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={tester && tester.address2}
                        hintText="Address two"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="city"
                        floatingLabelText="City"
                        fullWidth
                        maxLength="5"
                        type="number"
                        onChange={this.handleInput}
                        defaultValue={tester && tester.city}
                        hintText="City"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="state"
                        maxLength="2"
                        fullWidth
                        floatingLabelText="State"
                        onChange={this.handleInput}
                        defaultValue={tester && tester.state}
                        hintText="State"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="zipCode"
                        floatingLabelText="Zip code"
                        fullWidth
                        maxLength="5"
                        onChange={this.handleInput}
                        defaultValue={tester && tester.zipCode}
                        hintText="Zip code"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="model"
                        floatingLabelText="Model"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={tester && tester.model}
                        hintText="Model"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="serialNo"
                        floatingLabelText="Serial Number"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={tester && tester.serialNo}
                        hintText="Serial Number"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="make"
                        floatingLabelText="Make"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={tester && tester.make}
                        hintText="Make"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <DatePicker
                        floatingLabelText="Certificate Expires"
                        fullWidth
                        onChange={this.handleDateInput.bind(this, 'certExp')}
                        value={this.state.certExp}
                        hintText="Certificate Expires"
                    />
                </Col>

                <Col xs={12} md={3} sm={6}>
                    <TextField
                        ref="calibratedBy"
                        floatingLabelText="Calibrated By"
                        fullWidth
                        onChange={this.handleInput}
                        defaultValue={tester && tester.calibratedBy}
                        hintText="Calibrated By"
                    />
                </Col>
                <Col xs={12} md={3} sm={6}>
                    <DatePicker
                        floatingLabelText="Calibration Expires"
                        fullWidth
                        onChange={this.handleDateInput.bind(this, 'calibrationExp')}
                        value={this.state.calibrationExp}
                        hintText="Calibration Expires"
                    />
                </Col>
                <Col xs={12} md={12} sm={12}>
                    <TextField
                        ref="comment"
                        fullWidth
                        multiLine
                        rows={2}
                        floatingLabelText="Comment"
                        onChange={this.handleInput}
                        defaultValue={tester && tester.comment}
                        hintText="comment"
                    />
                </Col>
                <Col xs={12} md={3} sm={2}>
                    <Checkbox
                        label="Active"
                        onCheck={this.handleCheckBoxInput.bind(this, 'active')}
                        defaultChecked={tester && tester.active}
                    />
                </Col>
                <Col xs={12} md={3} sm={2}>
                    <Checkbox
                        label="Tester"
                        onCheck={this.handleCheckBoxInput.bind(this, 'tester')}
                        defaultChecked={tester && tester.tester}
                    />
                </Col>
                <Col xs={12} md={3} sm={2}>
                    <Checkbox
                        label="Inspector"
                        onCheck={this.handleCheckBoxInput.bind(this, 'inspector')}
                        defaultChecked={tester && tester.inspector}
                    />
                </Col>
                <Col xs={12} md={3} sm={2}>
                    <Checkbox
                        label="Installer"
                        onCheck={this.handleCheckBoxInput.bind(this, 'installer')}
                        defaultChecked={tester && tester.installer}
                    />
                </Col>
            </Row>
        );
    }
}

TesterEditor.propTypes = {
  tester: PropTypes.object,
};
