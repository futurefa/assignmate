# The Meteor Chef - Base
A starting point for Meteor apps.
[Read the Documentation](http://themeteorchef.com/base)


Run `meteor npm install`

Run `meteor`

##Packages
* [Material UI](http://www.material-ui.com/)
* [React FlexBox Grid](http://roylee0704.github.io/react-flexbox-grid/)
* [formsy-material-ui](https://github.com/mbrookes/formsy-material-ui/blob/master/src/FormsyText.jsx)
* View the full list of packages [here](https://themeteorchef.com/base/packages-included) (except Bootstrap packages)

<table>
  <tbody>
    <tr>
      <th>Base Version</th>
      <td>v4.15.0</td>
    </tr>
    <tr>
      <th>Meteor Version</th>
      <td>v1.4.4.1</td>
    </tr>
  </tbody>
</table>
